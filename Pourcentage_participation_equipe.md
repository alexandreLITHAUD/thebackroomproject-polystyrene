# Projet Groupe 4 - Polystyrène

## Répartition des points 

| Nom        | Prénom    | Participation (en %) |
| ---------- | --------- | -------------------- |
| FERRARI    | Julien    | 12.5%                |
| THOMAZO    | Corentin  | 12.5%                |
| BRUN       | Samuel    | 12.5%                |
| LITHAUD    | Alexandre | 12.5%                |
| BONFILS    | Antoine   | 12.5%                |
| PUECH      | Lilian    | 12.5%                |
| AVANTURIER | Aurélie   | 12.5%                |
| GUYOT      | Romain    | 12.5%                |
