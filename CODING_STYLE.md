STYLE DE CODE
========
> *Rappel:*
> *camelcase: tmpValue*
> *snakecase: tmp_value*

# 

* **Attribution des noms:**

|             | **Class** | **Function** | **Variable** |
| ----------- | --------- | ------------ | ------------ |
| **style**   | camelcase (avec majuscule au début) | camelcase    | camelcase    |
| **langage** | anglais   | anglais      | anglais      |
| *Exemple*   | FireCase  | getValue()   |  xValue;|

* Commentaire: **français**

* Toujours mettre le **this.** pour les attributes/méthodes dans une classe.

* Toujours faire **ctrl+shift+F** avant tout.

* **Langue** : Tout est en anglais sauf les commentaires qui sont faits en français. 

# 

* CODE POUR LES ACCOLADES

* L'accolade **ouvrante toute seule** est à la fin de la ligne pour les fonctions, les classes, et toutes les structures (if, else, switch, for, while, ...)
*Exemple:*
```JAVA
if (cond1) {
    ...
} else if (cond2) {
    ...
} 
```
* OBLIGATION de mettre des accolades même pour les structures à une seule ligne.
*Exemple:*
```JAVA
if (cond1) {
    ...
}
```

**EXEMPLE DE CLASSE**

```JAVA
package info3.game;

import java.util.ArrayList;

//    Indentation automatique avec Ctrl+Shift+F

public class CodingStyle {

    static final int const_var = 0;

    private int tempValue; // camelCase
    protected int x;
    public int y;

    private ArrayList<Integer> list = new ArrayList<>();

    CodingStyle(int tempValue, int x, int y) {
        this.tempValue = tempValue; // this OBLIGATOIRE
        this.x = x;
        this.y = y;
    }

    public int getValue(int paramOpt) {
        int temp = paramOpt;

        if (x == 0) {
            //
        } else if ((x < 0) && (y > 0)) {
            //
        } else {
            //
        }

        // =============================

        while (true) {
            //
            break;
        }

        return temp;
    }

}
```
