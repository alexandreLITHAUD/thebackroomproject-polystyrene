# PROPOSITION DE JEU - Groupe 4 : Polystyrène

# TheBackroomProject
> Aurélie Avanturier, Antoine Bonfils, Samuel Brun, Romain Guyot, Alexandre Lithaud, Lilian Puech, Corentin Thomazo.

## Table des matières : 
- [PROPOSITION DE JEU - Groupe 4 : Polystyrène](#proposition-de-jeu---groupe-4--polystyrène)
- [TheBackroomProject](#thebackroomproject)
  - [Table des matières :](#table-des-matières-)
  - [Fonctionnement du jeu :](#fonctionnement-du-jeu-)
    - [Monde:](#monde)
    - [Fonctionnement personnage :](#fonctionnement-personnage-)
    - [But du jeu :](#but-du-jeu-)
    - [Mécaniques particulières : (Originalité)](#mécaniques-particulières--originalité)
    - [`[DEMO]` Passage d'un mode à un autre  :](#demo-passage-dun-mode-à-un-autre--)
  - [Description générale du jeu :](#description-générale-du-jeu-)
  - [Affichage :](#affichage-)
  - [Menu :](#menu-)
  - [`[DEMO]` Ennemis [définis par des automates, modifiables dans le menu]](#demo-ennemis-définis-par-des-automates-modifiables-dans-le-menu)
  - [TODO définir les actions POP et WIZZ :](#todo-définir-les-actions-pop-et-wizz-)
  - [Liste des features :](#liste-des-features-)
  - [Description plus précise des features principales :](#description-plus-précise-des-features-principales-)
    - [Jauge de vie :](#jauge-de-vie-)
    - [Jauge de satiété :](#jauge-de-satiété-)
    - [Structure du labyrinthe :](#structure-du-labyrinthe-)
    - [Salles :](#salles-)
    - [Maps, Seed et Random :](#maps-seed-et-random-)
    - [Loots et vivres au sol :](#loots-et-vivres-au-sol-)
    - [Réapartion des ennemies :](#réapartion-des-ennemies-)
    - [Choix d'un mode de difficulté :](#choix-dun-mode-de-difficulté-)
    - [Game over et restart le jeu :](#game-over-et-restart-le-jeu-)
    - [2 biomes (classique et eau) :](#2-biomes-classique-et-eau-)
    - [Extension: Jauge de santé mentale :](#extension-jauge-de-santé-mentale-)
    - [Extension: Brouillard derrière les murs:](#extension-brouillard-derrière-les-murs)
  - [Réponse aux contraintes :](#réponse-aux-contraintes-)

## Fonctionnement du jeu :

### Monde:
Le personnage principal doit survivre dans une succession de labyrinthes, appelés étages. Chaque étage est peuplé de monstres hostiles. Un étage correspond à un environnement/biome (étage d'eau, de feu ou classique).

### Fonctionnement personnage :
Le personnage a une jauge de vie et une jauge de satiété. La jauge de satiété se vide au fur et à mesure que le temps passe (plus ou moins vite en fonction du biome). Pour la régénérer, il faudra récupérer des items particuliers (rations, bouteilles…).
La jauge de vie se vide quand le joueur prend des coups. Il peut la récupérer quand il récupère des trousses de soin. Si la jauge de vie tombe à 0, le joueur meurt. Si la jauge de satiété tombe à zéro, la jauge de vie se vide progressivement tous les n ticks tant que celle de satiété reste vide.

### But du jeu :
Pour sortir du labyrinthe et passer à l’étage suivant, il faut trouver la sortie. Il existe une sortie unique par étage qui se trouve à un endroit aléatoire de l’étage. Il faut donc survivre aux différents monstres se trouvant dans l'étage.

### Mécaniques particulières : (Originalité)
`[DEMO]` Deux modes de jeu distincts:
    *Mode de jeu 1 :*
2 joueurs = **1 seule entité**:
- le joueur 1 peut déplacer le personnage principal
- le joueur 2 peut tirer 
J1 => Mouvement (clavier)
J2 => Tir (souris)  

*Mode de jeu 2 :*
2 joueurs = **2 entités**:
- un personnage: peut se déplacer et se battre au corps à corps J1
- un personnage ennemi possédé qui peut se déplacer et se battre au corps à corps contrôlé par J2

J1 => Mouvement + Taper Corps à corps (clavier)
J2 => Mouvement + Taper Corps à corps (clavier)

### `[DEMO]` Passage d'un mode à un autre  :
- Mode 1 => Mode 2 :
    Si le joueur reçoit des dégats par un mob => J1 garde le contrôle du joueur
    J2 prend le contrôle du mob
- Mode 2 => Mode 1:
    Quand J2 meurt ou quand J2 sort du viewport, il fusionne avec J1 
    La jauge de vie de J1 et J2, fusionnée, retrécie.

## Description générale du jeu :

`[DEMO]` Des labyrinthes de *taille finie sans bord* (**tore**) et carrée sont générés aléatoirement les uns après les autres au fur et à mesure qu’on passe d’étage en étage.
Le but dans chaque étage est de trouver l’escalier/**le portail** (unique) qui mène vers l’étage suivant. Le but du jeu entier est d’aller le plus loin possible.

Le premier étage est un labyrinthe par défaut, dit "classique" afin de préparer le joueur aux étages suivants qui l’attendent. Les étages qui suivent sont soit “classique”, soit “d’eau”, 
`[DEMO]` 2 mécaniques de jeu différentes où le joueur n’agît pas de la même façon. Un troisième biome pourrait être ajouté si notre efficacité nous le permet : "de feu".

Le jeu vacille entre deux modes M1 et M2 :

**M1:** Les déplacements du joueur sont gérés par J1 qui contrôle le clavier, tandis que J2 contrôle la souris pour faire tirer le joueur dans la direction pointée sur la map. J1 et J2 sont donc fusionnés .
**M2:** J1 et J2 se splittent entre le joueur lui-même, toujours contrôlé par J1 au clavier, et J2 qui contrôle sa possession via le clavier, utilisant les flèches directionnelles pour se déplacer.

![Automate des modes de jeux](./images/automate_base.png)

## Affichage :
Le jeu sera affiché au travers d'un **viewport** de 17x11 cases qui ne montrera qu'une partie de la map au joueur.

## Menu :
- Sélection des automates pour les attribuer à des entitées 
- Sélection des musiques pour choisir sa musique préférée
- Ajustement du volume

## `[DEMO]` Ennemis [définis par des automates, modifiables dans le menu]
* **Insanity**: monstre de type zombie classique avançant lentement avec actions basiques (frapper devant soi)
* **Hound**: monstre se déplaçant rapidement et pouvant sauter sur le joueur s'il est proche
* **Slime**: monstre résistant se divisant en version miniatures quand tué jusqu'à ce qu'il ne puisse plus se diviser

## TODO définir les actions POP et WIZZ :

Ennemie > Pop = Mouvement ?
Ennemie > Wizz = Action Spéciale de l'ennemie en question ?
> Par exemple pour le hound ce sera une charge sur plusieur case

Player > Pop = Mouvement ?
Player > Wizz = Frapper ? 

## Liste des features :

|           A faire absolument           |                 Optionel                  |                  Optionel++                  |
|:--------------------------------------:|:-----------------------------------------:|:--------------------------------------------:|
|            2 Joueurs (COOP)            |      jauge de santé mentale (stress)      |            Système de sauvegarde             |
|              jauge de vie              |               3-5 monstres                |           Système de money + shop            |
|            jauge de satiété            | armes supplémentaires (système de portée) |            Tirer attire les mobs             |
|   Génération des maps en aléatoires    |       Loot via mobs pour les armes        |       Animations pause / ne pas bouger       |
|         Labyrinthe (couloirs)          |                   Boss                    |        piège avec lave qui se répand         |
|                 Salles                 |            améliorer les armes            | entité mur qui peut se lever et se rétracter |
| Boucle verticale et horizontale (Tore) |                inventaire                 |                                              |
|      2 monstres (zombie + hound )      |                  armures                  |                                              |
|         maps + seed + randoms          | Système de scoring via temps et/ou points |                                              |
|           Loots et vivres au sol       |            munitions limitées             |                                              |
|            Réapartion des ennemies      |       Autres biomes (Chaleur, etc.)       |                                              |
|     choix d'un mode de difficulté      |                 Brouillard derrière les murs                        |                                        |
|      game over et restart le jeu       |                                           |                                              |
|        2 biomes (classique + eau)      |                                           |                                              |

`[DEMO]` [ + à faire absolument : menu d'attribution des automates ]  

## Description plus précise des features principales :

Quelques éléments du tableau ci-dessus sont déjà détaillés plus haut dans le contrat. Nous nous penchons ici sur les autres points peut-être encore imprécis.

### Jauge de vie :
La jauge de vie est une jauge partagée par les deux joueurs qui est fusionnée en M1 et séparée en deux sous-jauges de vie en M2. En M1, si la jauge de vie tombe à zéro, le game over se déclenche. Lors du passage de M1 à M2, la jauge de vie se sépare en deux, donnant la proportion p à J1 et la proportion (1-p) à J2, p étant un chiffre encore non décidé.

### Jauge de satiété :
La jauge de satiété se vide avec le temps et se remplit lorsque J1 mange. Des lootables seront récoltables au sol et parmi eux de la nourriture afin que J1 ne meurt pas de fin. La jauge de satiété diminue également à chaque pas effectué.

### Structure du labyrinthe :
Chaque labyrinthe est généré avec un niveau de facilité n (entier) qui représente la largeur des couloirs. Lors de la création d'un labyrinthe, un gruyère à trous de taille n est généré, puis les trous se relient entre eux jusqu'à ce que le labyrinthe soit connexe par chemins. Quelques salles sont creusées à la fin de la génération du labyrinthe.

`[DEMO]` [niveau de difficulté du labyrinthe paramètrable dans le menu du jeu]

### Salles :
Au début du jeu, les joueurs apparaissent dans une salle: c'est la salle du spawn. 
`[DEMO]` Les autres salles permettent de faire apparaître plusieurs monstres et des monstres qui peuvent se "egg" à l'infini.

### Maps, Seed et Random :
Le joueur pourra connaitre la seed à pourra connaitre la seed à partir de laquelle le labyrinthe est créer.Le joueur en début de partie pourra entrer une seed. [bonne idée et pratique pour déboguer]

### Loots et vivres au sol :
Le jeu aura de manière aléatoire des vivre au sol permettant de faire remonter la jauge de satiété et la jauge de vie. De plus, certains ennemies feront tomber des vivre (loot) à leur mort.

### Réapartion des ennemies :
Il devra y avoir un certain nombre d'entité ennemie sur la map. Si cette valeur diminue, (si une a été éliminé par exemple) alors une autre entité devrai apparaître sur la caretd e jeu (spawn). 

### Choix d'un mode de difficulté :
Le joueur, dans le menu du début, choisit un mode de difficulté qui définira :
- la largeur des couloir
- la taille de la map
- les dégats ennemis
- les dégats du joueurs
- le taux de loot des ennemis vaincus

### Game over et restart le jeu :
Lorsque le joueur n'a plus de vie, le jeu s'arrête et affiche le nombre d'étages parcouru. Le menu propose de recommencer la partie, soit à l'étage 0, soit à l'étage actuel si vous avez choisi le mode Facile avant de démarrer le jeu plus tôt.

### 2 biomes (classique et eau) :
`[DEMO]` Le jeu comportera des avatars et des décors différents pour les deux biomes, l'un classique de type donjon et l'autre avec l'eau avec un comportement différent des joueurs et des ennemis.

### Extension: Jauge de santé mentale :
Une jauge qui diminue avec le temps et qui agit sur le champ de vision du joueur, plus elle est basse moins il va loin.

### Extension: Brouillard derrière les murs:
Le joueur continue à voir la strucutre du labyrinthe mais ne peut pas voir les monstres s'il ne sont pas dans la même salle que lui.

## Réponse aux contraintes :

| Contraintes | Réponses                                                                       |
| ----------- | ------------------------------------------------------------------------------ |
| Transfert   | Possession de l'ennemi par un joueur [OK] `[DEMO]`                             |
| Egg         | Monstre de type slime [OK] `[DEMO]`                                |
| Doublure    | Physiques différentes selon l'étage (Classique, Eau, Chaleur...) [ok] `[DEMO]` |
|             |                                                                                |

