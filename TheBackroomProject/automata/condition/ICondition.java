package condition;

import java.util.ArrayList;

import model.entity.Entity;

public interface ICondition {

	public boolean eval(Entity e);
	
	

}
