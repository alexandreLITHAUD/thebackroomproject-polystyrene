package condition;

import java.util.List;

import model.entity.Entity;

public class CondTrue implements ICondition{

	private List<String> param;
	
	public CondTrue(List<String> parametre) {
		this.param = parametre;
	}

	/**
	 * @param Entity e
	 * @return true
	 * 
	 */
	@Override
	public boolean eval(Entity e) {
		return true;
	}

}
