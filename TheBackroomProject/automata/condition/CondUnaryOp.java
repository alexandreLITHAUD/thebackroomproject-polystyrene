package condition;

import model.entity.Entity;

public class CondUnaryOp implements ICondition{
	private String operator;
	private ICondition exp;
	
	public CondUnaryOp(String op,ICondition exp) {
		this.operator=op;
		this.exp=exp;
	}
	
	/**
	 * @param Entity e
	 * @return boolean
	 * vérifie l'opérateur et change le résultat de exp.eval
	 */
	@Override
	public boolean eval(Entity e) {
		switch(this.operator) {
		case "!":
			return !this.exp.eval(e);
		}
		return this.exp.eval(e);
	}
}
