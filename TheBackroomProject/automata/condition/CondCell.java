package condition;

import java.util.ArrayList;
import java.util.List;

import model.Case;
import model.TypeCase;
import model.entity.Enemy;
import model.entity.Entity;
import model.entity.Player;
import model.entity.TypeEntity;
import model.loot.Loot;
import utils.MathUtils;

public class CondCell implements ICondition{

	private List<String> param;
	
	public CondCell(List<String> parametre) {
		this.param = parametre;
	}
	
	/**
	 * @param Entity e
	 * @return boolean
	 * vérifie les case autour de e en focntion de la direction et du type
	 */
	@Override
	public boolean eval(Entity e) {

		// Si il n'y a pas de parametre alors il cherche si il n'y a rien au dessus
		if(this.param == null || this.param.size() <= 1) {
			this.param.add(0, "N");
			this.param.add(1,"V");
		}
		
		String param1 = this.param.get(0);
		String param2 = this.param.get(1);
		
		Case c;
		
		int tailleMaxCol = e.getMap().getNbCol();
		int tailleMaxLine = e.getMap().getNbLine();

		switch(param1) {
		case "H":
			c = e.getMap().getCaseAt(e.getPosLine(), e.getPosCol());
			break;
		case "N":
		case "F":			
			c = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()-1, tailleMaxLine), e.getPosCol());
			break;
		case "E":
		case "R":
			c = e.getMap().getCaseAt(e.getPosLine(),MathUtils.realModulo(e.getPosCol()+1, tailleMaxCol));
			break;
		case "W":
		case "L":
			c = e.getMap().getCaseAt(e.getPosLine(),MathUtils.realModulo(e.getPosCol()-1, tailleMaxCol));
			break;
		case "S":
		case "B":
			c = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()+1, tailleMaxLine), e.getPosCol());
			break;
		default:
			c = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()-1, tailleMaxLine), e.getPosCol());
			break;
		}
		
		switch(param2) {
		case "V":// vide
			return ((c.getTypeCase() == TypeCase.NORMALGROUND || c.getTypeCase() == TypeCase.WATERGROUND || c.getTypeCase() == TypeCase.FIREGROUND || c.getTypeCase() == TypeCase.PORTAL) && !c.hasGameItem());
		case "O":// obstacle
			return (c.getTypeCase() == TypeCase.WALL);
		case "A"://ennemie
			return (c.hasGameItem() && !(c.getGameItem() instanceof Loot ) && (((Entity)c.getGameItem()).getTypeEntity() == TypeEntity.HOUND || ((Entity)c.getGameItem()).getTypeEntity() == TypeEntity.ZOMBIE || ((Entity)c.getGameItem()).getTypeEntity() == TypeEntity.SLIME));
		case "@":// player
		case "T"://allié
			return (c.getGameItem() instanceof Player); 
		case "_":
			return (c.hasGameItem());
		case "P":// est un item récupérable
			return (c.getGameItem() instanceof Loot);
		default:
			return false;
		}
		
		
	}
}
