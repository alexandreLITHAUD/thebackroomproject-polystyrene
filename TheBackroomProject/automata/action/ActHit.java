package action;

import java.util.List;

import model.entity.Entity;

public class ActHit implements IAction{
	private int percent;
	private List<String> param;
	
	public ActHit(List<String> parametre,int percent) {
		this.param=parametre;
		this.percent=percent;
	}
	
	public int getpercent() {
		return this.percent;
	}
	
	/**
	 * @param Entity e
	 * @return boolean
	 * Apelle hit dans l'Entity e en fonction de la direction
	 */
	@Override
	public boolean apply(Entity e) {
		// Si il n'y a pas de parametres
		if(this.param.size() == 0 || this.param == null) {
			return e.hitUp();
		}
		
		// si il y a des paramêtre a gérer
		switch(this.param.get(0)) {
		
		case "N":
		case "F":
			return e.hitUp();
		
		case "E":
		case "R":
			return e.hitRight();
			
		case "W":
		case "L":
			return e.hitLeft();
			
		case "S":
		case "B":
			return e.hitDown();
			
		case "H":
			return e.hitHere();
			
		default:
			return false;
		}
	}
	
}
