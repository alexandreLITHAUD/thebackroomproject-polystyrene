package action;

import java.util.List;

import model.entity.Entity;

public class ActThrow implements IAction {
	private int percent;
	private List<String> param;

	public ActThrow(List<String> parametre, int percent) {
		this.param = parametre;
		this.percent = percent;
	}

	public int getpercent() {
		return this.percent;
	}
	
	/**
	 * @param Entity e
	 * @return boolean
	 * Apelle la fonction paintGround
	 * qui permet de peindre le sol en croix et en rond
	 */
	@Override
	public boolean apply(Entity e) {
		e.paintGround();
		return true;
	}

}
