package action;

import java.util.List;

import model.entity.Entity;
import model.entity.Player;

public class ActPick implements IAction{
	private int percent;
	private List<String> param;
	
	public ActPick(List<String> parametre,int percent) {
		this.param=parametre;
		this.percent=percent;
		
	}
	public int getpercent() {
		return this.percent;
	}

	
	/**
	 * @param Entity e
	 * @return boolean
	 * Apelle la fonction pick dans playerS
	 */
	@Override
	public boolean apply(Entity e) {
	// 
	if(e instanceof Player) {
		Player player=(Player)e;
		if(this.param.size() == 0 || this.param == null) {
			return player.pickUp();
		}
	
	// si il y a des paramêtre a gérer
			switch(this.param.get(0)) {
			
			case "N":
			case "F":
				return player.pickUp();
			
			case "E":
			case "R":
				return player.pickRight();
				
			case "W":
			case "L":
				return player.pickLeft();
			case "S":
			case "B":
				return player.pickDown();
				
			default:
				return false;
			}
		} else {
			return false;
		}
	}
	

}
