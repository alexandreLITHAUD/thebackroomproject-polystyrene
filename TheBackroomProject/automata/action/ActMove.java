package action;

import java.util.List;
import java.util.Random;

import model.entity.Entity;

public class ActMove implements IAction{
	private int percent;
	private List<String> param;
	
	public ActMove(List<String> parametre,int percent) {
		this.param=parametre;
		this.percent=percent;
	}

	
	public int getpercent() {
		return this.percent;
	}
	
	/**
	 * @param Entity e
	 * @return boolean
	 * Apelle la fonction moveUp dans Entity e
	 */
	@Override
	public boolean apply(Entity e) {
		
		// Si il n'y a pas de parametres
		if(this.param.size() == 0 || this.param == null) {
			return e.moveUp();
		}
		
		// si il y a des paramêtre a gérer
		switch(this.param.get(0)) {
		
		case "N":
		case "F":
			return e.moveUp();
		
		case "E":
		case "R":
			return e.moveRight();
			
		case "W":
		case "L":
			return e.moveLeft();
		case "S":
		case "B":
			return e.moveDown();
			
		default:
			return false;
		}
	}

}
