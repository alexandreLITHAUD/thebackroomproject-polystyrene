package automataEngine;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import action.ActEgg;
import action.ActExplode;
import action.ActGet;
import action.ActHit;
import action.ActJump;
import action.ActMove;
import action.ActPick;
import action.ActPop;
import action.ActPower;
import action.ActProtect;
import action.ActStore;
import action.ActThrow;
import action.ActTurn;
import action.ActWait;
import action.ActWizz;
import action.IAction;
import ast.AST;
import ast.Action;
import ast.Automaton;
import ast.Behaviour;
import ast.BinaryOp;
import ast.Category;
import ast.Condition;
import ast.Direction;
import ast.FunCall;
import ast.IVisitor;
import ast.Key;
import ast.Mode;
import ast.State;
import ast.Transition;
import ast.UnaryOp;
import ast.Underscore;
import ast.Value;
import condition.CondBinaryOp;
import condition.CondCell;
import condition.CondClossest;
import condition.CondGotPower;
import condition.CondGotStuff;
import condition.CondKey;
import condition.CondMyDir;
import condition.CondTrue;
import condition.CondUnaryOp;
import condition.ICondition;
import parser.AutomataParser;

public class AutBuilder implements IVisitor {

	private ArrayList<AutomatonExe> m_automata;
	private AutomatonExe m_automate;
	private StateExe currentState;

	public AutBuilder() {
	}

	/**
	 * 
	 * @param filename
	 * @return une liste d'automate
	 * parse le fichier et créer des automates executable à partir d'un ast
	 */
	public ArrayList<AutomatonExe> loadAutomata(String filename) {
		try {
			AST ast = (AST) AutomataParser.from_file(filename);
			this.m_automata = new ArrayList<>();
			ast.accept(this);
			return this.m_automata;

		} catch (Exception ex) {
			return null;
		}
	}

	// THE METHODS REQUIRED BY IVisitor
	
	/**
	 * @param
	 * @return string 
	 * get string de cat
	 * 
	 */
	public Object visit(Category cat) {
		return cat.toString();
	}
	
	/**
	 * @param
	 * @return string 
	 * get string de dir
	 * 
	 */
	public Object visit(Direction dir) {
		return dir.toString();
	}
	
	/**
	 * @param
	 * @return string 
	 * get string de key
	 * 
	 */
	public Object visit(Key key) {
		return key.toString();
	}
	
	/**
	 * @param
	 * @return string 
	 * get string de value
	 * 
	 */
	public Object visit(Value v) {
		return v.toString();
	}

	/**
	 * @param
	 * @return string 
	 * get string de underscore
	 * 
	 */
	public Object visit(Underscore u) {
		return u.toString();
	}
	
	public void enter(FunCall funcall) {
	}

	/**
	 * @param funcall (format ast) et params une liste de string
	 * @return ICndiont ou une IAction
	 * créer une IAction et une ICondition en fonction du name de funcall
	 * ajoute des paramètres
	 */
	public Object exit(FunCall funcall, List<Object> params) {
		List<String> paramTemp = (List) params;
		int percent = funcall.percent;
		if (funcall != null) {
			String temp = funcall.name;
			switch (temp) {
			case "Cell":
				return (ICondition) new CondCell(paramTemp);
			case "Key":
				return (ICondition) new CondKey(paramTemp);
			case "True":
				return (ICondition) new CondTrue(paramTemp);
			case "MyDir":
				return (ICondition) new CondMyDir(paramTemp);
			case "Closest":
				return (ICondition) new CondClossest(paramTemp);
			case "GotPower":
				return (ICondition) new CondGotPower(paramTemp);
			case "GotStuff":
				return (ICondition) new CondGotStuff(paramTemp);
			case "Egg":
				return (IAction) new ActEgg(paramTemp, percent);
			case "Get":
				return (IAction) new ActGet(paramTemp, percent);
			case "Hit":
				return (IAction) new ActHit(paramTemp, percent);
			case "Jump":
				return (IAction) new ActJump(paramTemp, percent);
			case "Explode":
				return (IAction) new ActExplode(paramTemp, percent);
			case "Move":
				return (IAction) new ActMove(paramTemp, percent);
			case "Pick":
				return (IAction) new ActPick(paramTemp, percent);
			case "Pop":
				return (IAction) new ActPop(paramTemp, percent);
			case "Power":
				return (IAction) new ActPower(paramTemp, percent);
			case "Protect":
				return (IAction) new ActProtect(paramTemp, percent);
			case "Store":
				return (IAction) new ActStore(paramTemp, percent);
			case "Turn":
				return (IAction) new ActTurn(paramTemp, percent);
			case "Throw":
				return (IAction) new ActThrow(paramTemp, percent);
			case "Wait":
				return (IAction) new ActWait(paramTemp, percent);
			case "Wizz":
				return (IAction) new ActWizz(paramTemp, percent);

			}
		}
		return null;
	}

	/**
	 * @param ast operator ICondition left et ICondition right
	 * @return ICondition
	 */
	public Object visit(BinaryOp operator, Object left, Object right) {
		return (ICondition) new CondBinaryOp(operator.operator, (ICondition) left, (ICondition) right);
	}
	
	/**
	 * @param ast operator ICondition exp
	 * @return ICondition
	 */
	public Object visit(UnaryOp operator, Object exp) {

		return (ICondition) new CondUnaryOp(operator.operator, (ICondition) exp);
	}
	
	

	/**
	 * @param ast state
	 * @return StateExe
	 * vérifie si le StateExe avec le nom du parmètre state existe
	 * si il existe : renvoie le StateExe correspondant 
	 * sinon : Créer un StateExe et le renvoie
	 */
	public Object visit(State state) {
		//test si il est présent
		for (StateExe teststate : m_automate.getstates()) {
			if (teststate.getName().equals(state.toString())) {
				return teststate;
			}
		}
		// Créer un nouvel StateExe et le rajoute à la liste
		StateExe newState =new StateExe(state.name);
		this.m_automate.add(newState);
		return newState;
	}

	public void enter(Mode mode) {

	}
	/**
	 * @param ast mode, StateExe source_state, List<TransitionExe> behaviour
	 * @return StateExe
	 * Ajoute au StateExe source_state ses TransitionExe
	 */
	
	public Object exit(Mode mode, Object source_state, Object behaviour) {

		StateExe state = (StateExe) source_state;
		this.currentState=state;
		for (Object te : (LinkedList<Object>) behaviour) {
			TransitionExe transition=(TransitionExe)te;	
			transition.setSource(state);
			state.add(transition);
		}
		return state;
	}
	
	public void enter(Condition condition) {
	}
	
	/**
	 * @param ast condition et ICondition exp
	 * @return ICondition
	 */
	public Object exit(Condition condition, Object exp) {

		return (ICondition) exp;
	}

	public void enter(Action action) {
	}
	
	/**
	 * @param ast action, List<IAction> funcalls
	 * @return List<IAction> funcalls
	 */
	public Object exit(Action action, List<Object> funcalls) {
		return funcalls;
	}

	/**
	 * @param ast transition, ICondition condition, IAction action, StateExe target
	 * @return TransitionExe
	 * Créer une nouvelle TransitionExe à partir des paramètre et d'un paramètre globale
	 */
	public Object visit(Transition transition, Object condition, Object action, Object target) {
		// Construit la TransitionExe
		TransitionExe te = new TransitionExe(this.currentState, (StateExe)target, (ICondition) condition);
		// ajout des actions 
		for(Object act:(LinkedList<Object> )action) {
			te.add((IAction)act);
		}
		return (TransitionExe) te;
	}
	
	/**
	 * @param ast behaviour, TransitionExe transitions
	 * @return TransitionExe
	 */
	public Object visit(Behaviour behaviour, List<Object> transitions) {
		return transitions;
	}

	/**
	 * @param ast automaton
	 * @return
	 * initialise le paramètre globale m_automate 
	 */
	public void enter(Automaton automaton) {
		this.m_automate = new AutomatonExe(automaton.name);
	}
	
	// récupère un StateExe et une liste de StateExe
	// initialise l'état initial de l'automate et l'ajoute à la liste
	/** 
	 * @param ast automaton, StateExe initial_state, List<StateExe> modes
	 * @return
	 */
	public Object exit(Automaton automaton, Object initial_state, List<Object> modes) {
		m_automate.initState((StateExe) initial_state);
		m_automata.add(m_automate);
		return null;
	}

	public Object visit(AST bot, List<Object> automata) {

		return null;
	}

}
