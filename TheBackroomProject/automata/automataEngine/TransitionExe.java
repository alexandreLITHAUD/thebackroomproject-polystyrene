package automataEngine;

import java.util.ArrayList;

import action.IAction;
import condition.ICondition;


public class TransitionExe {
	private StateExe source;
	private StateExe target;
	private ICondition condition;
	private ArrayList<IAction> listAction= new ArrayList<>();
	
	/**
	 * 
	 * @param source
	 * @param target
	 * @param condition
	 * Constructeur de TransitionExe
	 */
	public TransitionExe(StateExe source,StateExe target,ICondition condition){
		this.source=source;
		this.target=target;
		this.condition=condition;
	}
	
	/**
	 * 
	 * @param action
	 * Ajoute des actions à la transition
	 */
	public void add(IAction action) {
		this.listAction.add(action);
	}
	
	/**
	 * 
	 * @return StateExe
	 * get l'état source
	 */
	public StateExe getSource() {
		return source;
	}
	
	/**
	 * 
	 * @return StateExe
	 * get l'état cible
	 */
	public StateExe getTarget() {
		return target;
	}
	
	/**
	 * 
	 * @return ICondition
	 * get la condition
	 */
	public ICondition getCondition() {
		return condition;
	}
	
	/**
	 * 
	 * @return ArrayList<IAction>
	 * get la iste des actions
	 */
	public ArrayList<IAction> getAction() {
		return listAction;
	}
	
	/**
	 * 
	 * @param StateExe source
	 * set le StateExe de l'état source de la transition
	 */
	public void setSource(StateExe source){
		this.source=source;
	}
}
