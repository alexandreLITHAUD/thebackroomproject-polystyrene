package automataEngine;

import java.util.ArrayList;
import java.util.Random;

import action.IAction;
import model.entity.Entity;

public class StateExe {
	
	private String name;
	private ArrayList<TransitionExe> listTransition= new ArrayList<>();
	
	/**
	 *
	 * @param String name
	 * Constructeur de StateExe
	 */
	public StateExe(String name) {
		this.name=name;
	}
	
	/**
	 *
	 * @return String name
	 * get le nom de l'état
	 */
	public String getName(){
		return this.name;
	}
	
	/**
	 *
	 * @param TransitionExe newTransition
	 * Ajoute des transitions à l'état source
	 */
	public void add(TransitionExe newTransition) {
		listTransition.add(newTransition);
	}
	
	/**
	 *
	 * @param TransitionExe newTransition
	 * execute un pas de l'automate
	 */
    public void step(Entity e) {
        for(TransitionExe te : listTransition) {
        	/* test les ICondition
        	 * si plusieurs sont vraie la première est executer
        	 */
            if (te.getCondition().eval(e)) {
            	Random ramdom = new Random();
            	ArrayList<IAction> list=te.getAction();
				for(IAction act : list) {
					if(act.getpercent() != -1) {
						int value = ramdom.nextInt(100) + 1;
						if(value <= act.getpercent()) { 
							act.apply(e); 
						}// Dans ce cas le code continue
						else {  } // Si le tirage perd on fini l'action
					}
					else {
						act.apply(e);
					}
				}

                
                // gestion de l'aléatoire des états
                ArrayList<StateExe> listState=  e.getAutomata().getstates();
                /* vérifie si le '_' est le nom de l'état target
                 * si oui: prend un état au hasard
                 * sinon: attribue à current state l'état target
                 */
                if (te.getTarget().getName().equals('_')) {
                    int numberState=ramdom.nextInt(listState.size());
                    StateExe nextState= listState.get(numberState);
                    if(nextState.getName().equals('_')) {
                         nextState= listState.get(numberState+1);

                    }
                    e.setCurrentState(nextState);
                }else {
                    e.setCurrentState(te.getTarget());
                }

            return;
            }
        }
	}
	
	
	
	
}
