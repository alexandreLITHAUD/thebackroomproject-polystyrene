package controller;

import java.awt.Graphics;
import java.util.ArrayList;

import automataEngine.AutBuilder;
import automataEngine.AutomatonExe;
import automataEngine.StateExe;
import info3.game.CanvasListener;
import info3.game.Game;
import model.entity.Entity;
import model.entity.TypeEntity;
import vue.ViewController;

/**
 * MainController : gère le jeu, les automates et les actions avec l'extérieur
 * c'est cette classe qui connait le listener pour les inputs 
 */
public class MainController {
	/* connaissance du jeu et de la vue */
	private Game game;
	private ViewController view;
	/* Liste des automates disponibles */
	private ArrayList<AutomatonExe> automatons;
	/* Liste des inputs actuels */
	private ArrayList<Integer> inputs;
	/* écouteur d'action */
	private CanvasListener listener;
	
	
	/* Automates par défaut des entités */
	private String player1AutoName = "Player"; //joueur 1 seul
	public static String ombreAutoName   = "Ombre"; //joueur 2 ombre
	public static String ombrePossess = "OmbrePossess"; //joueur 2 possession
	private String jfusionAutoName = "PlayerFuse"; //joueur 1&2 fusion
	private String houndAutoName = "EnemyBaseHit"; //hound 
	private String zombieAutoName = "EnemyConfuHit"; //zombie
	private String slimeAutoName = "SlimeAttack"; //SLIME pour Egg 
	private String Missile="Bullet"; //balles
	
	/* automate par defaut */
	private AutomatonExe philosopher;

	/**
	 * Donne le nom de l'automate par défaut des balles
	 * 
	 * @return le nom de l'automate courant pour les balles
	 */
	public String getProjAutomaton() {
		return this.Missile;
	}
	
	/**
	 * set le nom de l'automate des projectiles
	 * 
	 * @param a le nouveau nom de l'automate pour les projectiles
	 */
	public void setProjAutomaton(String a) {
		this.Missile = a;
	}
	
	/**
	 * Création du controlleur
	 * 
	 * @param g le game qui crée le controlleur
	 */
	public MainController(Game g) {
		this.game = g;
		// Lecteur d'automates
		AutBuilder build = new AutBuilder();
		this.automatons = build.loadAutomata("./resources/automate/testAutomateSimple.gal");
		
		// Init du listener
		listener = new CanvasListener(this);

		// Initilaise le controlleur dans tous les automates
		for(AutomatonExe auto : this.automatons) {
			auto.setController(this);
		}
		
		
		//creation automate penseur par défaut
		this.philosopher = new AutomatonExe("philosopher");
		StateExe stateExe = new StateExe("init");
		this.philosopher.setController(this);
		this.philosopher.add(stateExe);
		this.philosopher.initState(stateExe);
		
	}
	
	/**
	 * Donne la liste des automates
	 * 
	 * @return la liste courante des automates
	 */
	public ArrayList<AutomatonExe> getAutomata() {
		return this.automatons;
	}
	
	/**
	 * Donne le listener actuel 
	 * 
	 * @return le listener
	 */
	public CanvasListener getListener() {
		return this.listener;
	}
	
	/**
	 * Set la vue du controlleur, elle est cette apres car au moment de la création du controlleur
	 * la vue n'est pas encore créée
	 * 
	 * @param view La vue a set
	 */
	public void setView(ViewController view) {
		this.view = view;
	}

	/**
	 * Set pour une entité l'automate par défaut
	 * 
	 * @param e l'entité dont on veut set l'automate
	 */
	public void setAutomata(Entity e) {
		
		AutomatonExe auto = null;
		
		/* En fonction de notre entité */
		switch(e.getTypeEntity()) {
		
		case TypeEntity.PLAYER1:
			
			auto = searchForAutomata(player1AutoName);
			
			break;
		case TypeEntity.OMBRE:
			
			auto = searchForAutomata(ombreAutoName);
			
			break;
		case TypeEntity.JFUSION:
			
			auto = searchForAutomata(jfusionAutoName);
			
			break;
		case TypeEntity.SLIME:
			
			auto = searchForAutomata(slimeAutoName);
			
			break;
		case TypeEntity.HOUND:
			
			auto = searchForAutomata(houndAutoName);
			
			break;
		case TypeEntity.ZOMBIE:
			
			auto = searchForAutomata(zombieAutoName);
			
			break;
			
		case TypeEntity.MISSILE:
			
			auto = searchForAutomata(Missile);
			
			break;
		default:
			/* si aucune entité n'est reconnue, il y a eu un probleme */
			auto = this.philosopher;
		}
		/* setting de l'automate de l'entité & de l'etat initial */
		e.setAutomata(auto);
		e.setCurrentState(auto.getinitialState());
		return;
	}
	
	/**
	 * Recherche un automate dans la liste a partie de son 
	 * 
	 * @param name le nom de l'automate que l'on cherche 
	 * @return l'automate qui porte le name
	 */
	private AutomatonExe searchForAutomata(String name) {
		for(AutomatonExe auto : this.automatons) {
			if(auto.getName().equals(name)) { 
				return auto; 
			}
		}
		System.err.println("No Automata found for automata :" + name);
		return this.philosopher;
	}
	
	/**
	 * set pour une entité son automate
	 * 
	 * @param e l'entité dont on vuet changer l'automate
	 * @param auto l'automate a mettre
	 */
	public void setAutomata(Entity e, AutomatonExe auto) {
		e.setAutomata(auto);
		e.setCurrentState(auto.getinitialState());
	}
	
	/**
	 * set pour une entité son automate
	 * 
	 * @param e l'entité dont on vuet changer l'automate
	 * @param nameAuto le nom de l'automate
	 */
	public void setAutomata(Entity e, String nameAuto) {
		AutomatonExe auto = searchForAutomata(nameAuto);
		e.setAutomata(auto);
		e.setCurrentState(auto.getinitialState());
	}
	
	/**
	 * tick
	 * 
	 * @param elapsed le temps entre 2 ticks
	 */
	public void tick(long elapsed) {
		if (!this.view.inMenu()) {
			game.tick(elapsed);
		}
		view.tick(elapsed);
	}
	
	/**
	 * paint
	 * 
	 * @param g la toile
	 */
	public void paint(Graphics g) {
		view.paint(g);
	}
	
	/**
	 * demande a la vue de charger la musique
	 */
	public void loadMusic() {
		view.loadMusic();
	}
	
	/**
	 * Redirige les cliques
	 * 
	 * @param x coordonnée en x du clique
	 * @param y coordonnée en y du clique
	 */
	public void mousePressed(int x,int y) {
		this.view.clickAs(x, y);
	}
	
	/**
	 * Redirige le release
	 * 
	 * @param x coordonnée en x du release
	 * @param y coordonnée en y du release
	 */
	public void mouseReleased(int x,int y, int button) {
		this.view.clickReleaseAs(x, y, button);
	}
	
	/**
	 * Redirige le mouvement de la sourie
	 * 
	 * @param x coordonnée en x de la sourie
	 * @param y coordonnée en y de la sourie
	 */
	public void mouseMove(int x, int y) {
		this.view.mouseMove(x, y);
	}
	
	/**
	 * Set et redirige les entrées
	 * 
	 * @param inputs les entrées actuelles 
	 */
	public void setInputs(ArrayList<Integer> inputs) {
		view.setInputs(inputs);
		this.inputs = inputs;
	}
	
	/**
	 * donne les inputs
	 * 
	 * @return les entrées actueles
	 */
	public ArrayList<Integer> getInputs(){
		return this.inputs;
	}
	
	/**
	 * donne le jeu
	 * 
	 * @return le jeu
	 */
	public Game getGame() {
		return this.game;
	}
}
