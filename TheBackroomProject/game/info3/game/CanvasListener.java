/*
 * Copyright (C) 2020  Pr. Olivier Gruber
 * Educational software for a basic game development
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: March, 2020
 *      Author: Pr. Olivier Gruber
 */
package info3.game;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import controller.MainController;
import info3.game.graphics.GameCanvasListener;

public class CanvasListener implements GameCanvasListener {
	MainController control;
	ArrayList<Integer> keypressed;

	public CanvasListener(MainController c) {
		this.control = c;
		keypressed = new ArrayList<>();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// System.out.println("Mouse clicked: ("+e.getX()+","+e.getY()+")");
		// System.out.println(" modifiers="+e.getModifiersEx());
		// System.out.println(" buttons="+e.getButton());

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// System.out.println("Mouse pressed: (" + e.getX() + "," + e.getY() + ")");
		// System.out.println(" modifiers=" + e.getModifiersEx());
		// System.out.println(" buttons=" + e.getButton());
		control.mousePressed(e.getX(), e.getY());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// System.out.println("Mouse released: (" + e.getX() + "," + e.getY() + ")");
		// System.out.println(" modifiers=" + e.getModifiersEx());
		// System.out.println(" buttons=" + e.getButton());
		control.mouseReleased(e.getX(), e.getY(),e.getButton());
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// System.out.println("Mouse entered: ("+e.getX()+","+e.getY()+")");
		// System.out.println(" modifiers="+e.getModifiersEx());
		// System.out.println(" buttons="+e.getButton());
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// System.out.println("Mouse exited: ("+e.getX()+","+e.getY()+")");
		// System.out.println(" modifiers="+e.getModifiersEx());
		// System.out.println(" buttons="+e.getButton());
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// System.out.println("Mouse dragged: ("+e.getX()+","+e.getY()+")");
		// System.out.println(" modifiers="+e.getModifiersEx());
		// System.out.println(" buttons="+e.getButton());
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// System.out.println("Mouse moved: ("+e.getX()+","+e.getY()+")");
		// System.out.println(" modifiers="+e.getModifiersEx());
		// System.out.println(" buttons="+e.getButton());
		control.mouseMove(e.getX(), e.getY());
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// System.out.println("Key typed: " + e.getKeyChar() + " code=" +
		// e.getKeyCode());
		/* ajout a la liste des boutons appuyés */
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		//System.out.println("Key pressed: " + e.getKeyChar() + " code=" + e.getKeyCode());
		if (!keypressed.contains((Integer) e.getKeyCode())) {
			keypressed.add((Integer) e.getKeyCode());
			control.setInputs(keypressed);
		}
		//générateur de carte aléatoire avec touche espace
//		if(e.getKeyCode() == 32) {
//			this.control.getGame().newStage();
//		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// System.out.println("Key released: "+e.getKeyChar()+" code="+e.getKeyCode());
		/* suppression de la liste des boutons appuyés */
		keypressed.remove((Integer) e.getKeyCode());
		control.setInputs(keypressed);
	}

	@Override
	public void tick(long elapsed) {
		control.tick(elapsed);
	}

	@Override
	public void paint(Graphics g) {
		control.paint(g);
	}

	@Override
	public void windowOpened() {
		control.loadMusic();
	}

	@Override
	public void exit() {
	}

//  boolean expired;
	@Override
	public void endOfPlay(String name) {
//    if (!expired) // only reload if it was a forced reload by timer
		control.loadMusic();
//    expired = false;
	}

	@Override
	public void expired() {
		// will force a change of music, after 6s of play
//    System.out.println("Forcing an ealy change of music");
//    expired = true;
//    game.loadMusic();    
	}

}