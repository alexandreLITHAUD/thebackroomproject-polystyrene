package utils;

import java.util.Random;

import model.Case;
import model.NormalGround;
import model.Portal;
import model.Wall;
import model.entity.Player;

public class MapFactoryMaze {
	private static final int FLAG_SPAWNER = 6;
	private static final int FLAG_ROOM = 5;
	private static final int FLAG_PORTAL = 4;
	private static final int FLAG_WALL = 3;
	private static final int FLAG_PLAYER2 = 2;
	private static final int FLAG_PLAYER1 = 1;
	private static final int FLAG_COULOIR = 0;

	private static final float WALL_BREAK_PROB = 0.005F;
	private static final float ROOM_PROB = 0.001F;
	private static final int ROOM_SIZE = 5;

	/**
	 * Il crée un labyrinthe
	 * 
	 * @param nbLine       nombre de lignes de la carte
	 * @param nbCol        le nombre de colonnes
	 * @param j1           le joueur 1
	 * @param j2           le joueur 2
	 * @param rand         un générateur de nombres aléatoires
	 * @return Une matrice de Case.
	 */
	public static Case[][] initMap(int nbLine, int nbCol, Player j1, Player j2, Random rand) {
		if (nbLine % 2 != 0 && nbCol % 2 != 0) {
			System.err.print("MapFactoryMaze: incorrect size");
			System.exit(1);
		}
		
		int[][] lab = init_labyrinth(nbLine, nbCol);
		Case[][] mapCase = new Case[nbLine][nbCol];

		int lineValue;
		int colValue;
		int Direction = -1;

		int countLanes = -1 * ((nbLine / 2) * (nbCol / 2) + 1);
		while (countLanes != -2) {
			lineValue = rand.nextInt((nbLine - 1));
			if (lineValue % 2 == 0) {
				lineValue++;
			}
			colValue = rand.nextInt((nbCol - 1));
			if (colValue % 2 == 0) {
				colValue++;
			}

			Direction = rand.nextInt(3);
			switch (Direction) {
			case 0: // Right
				if (lab[(lineValue + 2) % nbLine][colValue] != lab[lineValue][colValue]) {
					lab[(lineValue + 1) % nbLine][colValue] = lab[lineValue][colValue];
					countLanes++;

					/*
					 * propager la valeur de la case choisie aux cases appartenant au couloir de la
					 * case qui jouxte cette première.
					 */
					propagation(lab, nbLine, nbCol, countLanes, (lineValue + 2) % nbLine, colValue,
							lab[(lineValue + 2) % nbLine][colValue], lab[lineValue][colValue]);
				}
				break;

			case 1: // Back
				if (lab[lineValue % nbLine][(colValue + 2) % nbCol] != lab[lineValue][colValue]) {
					lab[lineValue % nbLine][(colValue + 1) % nbCol] = lab[lineValue][colValue];
					countLanes++;

					/*
					 * propager la valeur de la case choisie aux cases appartenant au couloir de la
					 * case qui jouxte cette première.
					 */
					propagation(lab, nbLine, nbCol, countLanes, lineValue, (colValue + 2) % nbCol,
							lab[lineValue][(colValue + 2) % nbCol], lab[lineValue][colValue]);
				}
				break;

			case 2: // Left
				if (lab[utils.MathUtils.realModulo(lineValue - 2, nbLine)][colValue] != lab[lineValue][colValue]) {
					lab[utils.MathUtils.realModulo(lineValue - 1, nbLine)][colValue] = lab[lineValue][colValue];
					countLanes++;

					/*
					 * propager la valeur de la case choisie aux cases appartenant au couloir de la
					 * case qui jouxte cette première.
					 */
					propagation(lab, nbLine, nbCol, countLanes, utils.MathUtils.realModulo(lineValue - 2, nbLine),
							colValue, lab[utils.MathUtils.realModulo(lineValue - 2, nbLine)][colValue],
							lab[lineValue][colValue]);
				}
				break;

			case 3: // Front
				if (lab[lineValue][utils.MathUtils.realModulo(colValue - 2, nbCol)] != lab[lineValue][colValue]) {
					lab[lineValue][utils.MathUtils.realModulo(colValue - 1, nbCol)] = lab[lineValue][colValue];
					countLanes++;

					/*
					 * propager la valeur de la case choisie aux cases appartenant au couloir de la
					 * case qui jouxte cette première.
					 */
					propagation(lab, nbLine, nbCol, countLanes, lineValue,
							utils.MathUtils.realModulo(colValue - 2, nbCol),
							lab[lineValue][utils.MathUtils.realModulo(colValue - 2, nbCol)], lab[lineValue][colValue]);
				}
				break;
			}
		}
		assignLanes(lab, nbLine, nbCol);
		createRooms(lab, nbLine, nbCol, rand);
		createMorePossibilities(lab, nbLine, nbCol, rand);
		toCase(mapCase, lab, nbLine, nbCol, j1, j2);
		return mapCase;
	}

	/**
	 * Il propage la valeur d'une cellule à ses voisines
	 * 
	 * @param lab        matrice du labyrinth
	 * @param nbLine     la hauteur de la matrice
	 * @param nbCol      la largeur de la matrice
	 * @param countLanes le compteur de salles indépendantes
	 * @param lineValue  la coordonnée x du cas courant
	 * @param colValue   coordonnée y du cas courant
	 * @param oldVal     la valeur du cas avant la propagation
	 * @param newVal     la nouvelle valeur du cas
	 * 
	 *  |X|X |X|X |X |X |	|X|X |X |X |X |X | 	|X|X |X |X |X |X |
	 *  |X|-1|X|-3|-3|-3|	|X|-1|-1|-3|-3|-3| 	|X|-1|-1|-1|-1|-1|
	 *  |X|X |X|X |X |X | ->|X|X |X |X |X |X |->|X|X |X |X |X |X | 
	 *  |X|-4|X|-5|X |-6| 	|X|-4|X |-5|X |-6| 	|X|-4|X |-5|X |-6|
	 *  |X|X |X|X |X |X | 	|X|X |X |X |X |X | 	|X|X |X |X |X |X |
	 *  |X|-7|X|-8|X |-9| 	|X|-7|X |-8|X |-9| 	|X|-7|X |-8|X |-9|
	 */
	private static void propagation(int[][] lab, int nbLine, int nbCol, int countLanes, int lineValue, int colValue,
			int oldVal, int newVal) {
		lab[lineValue][colValue] = newVal;

		if (lab[(lineValue + 2) % nbLine][colValue % nbCol] != newVal
				&& lab[(lineValue + 1) % nbLine][colValue % nbCol] != FLAG_WALL) {
			lab[(lineValue + 1) % nbLine][colValue] = newVal;

			/*
			 * propager la valeur de la case choisie aux cases appartenant au couloir de la
			 * case qui jouxte cette première.
			 */
			propagation(lab, nbLine, nbCol, countLanes, (lineValue + 2) % nbLine, colValue, oldVal, newVal);
		}

		if (lab[lineValue][(colValue + 2) % nbCol] != newVal && lab[lineValue][(colValue + 1) % nbCol] != FLAG_WALL) {
			lab[lineValue][(colValue + 1) % nbCol] = newVal;

			/*
			 * propager la valeur de la case choisie aux cases appartenant au couloir de la
			 * case qui jouxte cette première.
			 */
			propagation(lab, nbLine, nbCol, countLanes, lineValue, (colValue + 2) % nbCol, oldVal, newVal);
		}

		if (lab[utils.MathUtils.realModulo(lineValue - 2, nbLine)][colValue] != newVal
				&& lab[utils.MathUtils.realModulo(lineValue - 1, nbLine)][colValue] != FLAG_WALL) {
			lab[utils.MathUtils.realModulo(lineValue - 1, nbLine)][colValue] = newVal;

			/*
			 * propager la valeur de la case choisie aux cases appartenant au couloir de la
			 * case qui jouxte cette première.
			 */
			propagation(lab, nbLine, nbCol, countLanes, utils.MathUtils.realModulo(lineValue - 2, nbLine), colValue,
					oldVal, newVal);
		}

		if (lab[lineValue][utils.MathUtils.realModulo(colValue - 2, nbCol)] != newVal
				&& lab[lineValue][utils.MathUtils.realModulo(colValue - 1, nbCol)] != FLAG_WALL) {
			lab[lineValue][utils.MathUtils.realModulo(colValue - 1, nbCol)] = newVal;

			/*
			 * propager la valeur de la case choisie aux cases appartenant au couloir de la
			 * case qui jouxte cette première.
			 */
			propagation(lab, nbLine, nbCol, countLanes, lineValue, utils.MathUtils.realModulo(colValue - 2, nbCol),
					oldVal, newVal);
		}
	}

	/**
	 * Il change la valeur des cellules qui font partie du même groupe pour leur
	 * affecter la valeur du FLAG_COULOIR.
	 * 
	 * @param lab    matrice du labyrinth
	 * @param nbLine la hauteur de la matrice
	 * @param nbCol  la largeur de la matrice
	 * 
	 *  |X |X |X |X |X |X |		|X|X|X|X|X|X|
	 *  |X |-3|-3|-3|-3|-3|		|X|0|0|0|0|0|
	 *  |X |X |X |-3|X |X | ->	|X|X|X|0|X|X|
	 *  |-3|-3|X |-3|-3|-3| 	|0|0|X|0|0|0|
	 *  |X |-3|X |X |X |-3| 	|X|0|X|X|X|0|
	 *  |X |-3|-3|-3|X |-3| 	|X|0|0|0|X|0|
	 */
	private static void assignLanes(int[][] lab, int nbLine, int nbCol) {
		int tmp = lab[1][1];
		for (int i = 0; i < nbLine; i++) {
			for (int j = 0; j < nbCol; j++) {
				if (lab[i][j] == tmp) {
					lab[i][j] = FLAG_COULOIR;
				}
			}
		}
	}

	/**
	 * Il crée des salles dans le labyrinthe.
	 * 
	 * @param lab    matrice du labyrinth
	 * @param nbLine la hauteur de la matrice
	 * @param nbCol  la largeur de la matrice
	 * @param rand   nombre aléatoire
	 */
	private static void createRooms(int[][] lab, int nbLine, int nbCol, Random rand) {
		int countRoom = 0;
		int nbRoom = (int) ((nbLine * nbCol) * ROOM_PROB);
		nbRoom = Math.max(nbRoom, 3);
		while (countRoom < nbRoom) {
			int lineCoordRoom = rand.nextInt((nbLine - 1));
			int colCoordRoom = rand.nextInt((nbCol - 1));

			if (lab[lineCoordRoom][colCoordRoom] != FLAG_ROOM) {
				int height = rand.nextInt(ROOM_SIZE) + 2;
				int width = rand.nextInt(ROOM_SIZE) + 2;

				int tmpI, tmpJ;

				for (int i = -height; i <= height; i++) {
					for (int j = -width; j <= width; j++) {
						tmpI = utils.MathUtils.realModulo(lineCoordRoom + i, nbLine) % (nbLine - 1);
						tmpJ = utils.MathUtils.realModulo(colCoordRoom + j, nbCol) % (nbCol - 1);
						
						if (lab[tmpI][tmpJ] == FLAG_WALL) {
							lab[tmpI][tmpJ] = FLAG_COULOIR;
						}
					}
				}

				if (countRoom == 0) {
					lab[lineCoordRoom][colCoordRoom] = FLAG_PORTAL;
				} else if (countRoom == 1) {
					lab[lineCoordRoom][colCoordRoom] = FLAG_PLAYER1;
					lab[(lineCoordRoom + 1) % nbLine][colCoordRoom] = FLAG_PLAYER2;

				} else {
					lab[lineCoordRoom][colCoordRoom] = FLAG_ROOM;
				}
				countRoom++;
			}
		}
	}

	/**
	 * Il casse des murs de façon aléatoire dans le labyrinthe.
	 * 
	 * @param lab    matrice du labyrinth
	 * @param nbLine la hauteur de la matrice
	 * @param nbCol  la largeur de la matrice
	 * @param rand   nombre aléatoire
	 */
	private static void createMorePossibilities(int[][] lab, int nbLine, int nbCol, Random rand) {
		int countHole = 0;
		int nbHole = (int) ((nbLine * nbCol) * WALL_BREAK_PROB);
		while (countHole < nbHole) {
			int lineValue = rand.nextInt((nbLine - 1));
			if (lineValue % 2 == 1)
				lineValue--;
			int colValue = rand.nextInt((nbCol - 1));
			if (colValue % 2 == 1)
				colValue--;

			if (lab[lineValue][colValue] == FLAG_WALL) {
				lab[lineValue][colValue] = FLAG_COULOIR;
				countHole++;
			}
		}
	}

	/**
	 * Il remplit la matrice de Case
	 * 
	 * @param map    matrice du labyrinth de cases
	 * @param lab    matrice du labyrinth d'entiers
	 * @param nbLine la hauteur de la matrice
	 * @param nbCol  la largeur de la matrice
	 * @param j1     joueur1
	 * @param j2     joueur2
	 */
	private static void toCase(Case[][] map, int[][] lab, int nbLine, int nbCol, Player j1, Player j2) {
		for (int i = 0; i < nbLine; i++) {
			for (int j = 0; j < nbCol; j++) {
				switch (lab[i][j]) {
				case FLAG_WALL:
					map[i][j] = new Wall(i, j);
					break;
				case FLAG_PORTAL:
					map[i][j] = new Portal(i, j);
					break;
				case FLAG_SPAWNER:
				case FLAG_ROOM:
				case FLAG_COULOIR:
					map[i][j] = new NormalGround(i, j);
					break;
				case FLAG_PLAYER2:
					map[i][j] = new NormalGround(i, j);
					map[i][j].setGameItem(j2);
					j2.move(i, j);
					break;
				case FLAG_PLAYER1:
					map[i][j] = new NormalGround(i, j);
					map[i][j].setGameItem(j1);
					j1.move(i, j);
					break;
				default:
					System.err.println("MapFactoryMaze: flag unknown");
					System.exit(1);
					break;
				}
			}
			System.out.println("");
		}
	}

	/**
	 * Il initialise le labyrinthe avec des murs sur les colonnes et lignes paires
	 * 
	 * @param nbLine la hauteur de la matrice
	 * @param nbCol  la largeur de la matrice 
	 * _______________ 
	 * |X|X |X|X |X|X |
	 * |X|-1|X|-2|X|-3| 
	 * |X|X |X|X |X|X | 
	 * |X|-4|X|-5|X|-6| 
	 * |X|X |X|X |X|X |
	 * 
	 */
	private static int[][] init_labyrinth(int nbLine, int nbCol) {
		int[][] lab = new int[nbLine][nbCol];
		int cellValue = -1;
		for (int i = 0; i < nbLine; i++) {
			for (int j = 0; j < nbCol; j++) {
				if (i % 2 == 1 && j % 2 == 1) {
					lab[i][j] = cellValue;
					cellValue--;
				} else {
					lab[i][j] = FLAG_WALL;
				}
			}
		}
		return lab;
	}
}
