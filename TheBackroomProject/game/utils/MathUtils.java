package utils;

import model.TypeCase;
import model.entity.Entity;

public class MathUtils {

	/**
	 * Renvoie a % b dans l'intervalle [0,b[
	 * 
	 * @param a entier
	 * @param b modulo
	 */
	public static int realModulo(int a, int b) {
		if (a >= 0) {
			return a % b;
		} else {
			int res = a;
			while (res < 0) {
				res += b;
			}
			return res;
		}
	}
	/**
	 * pathfinding
	 * 
	 * @param prend une enité
	 * @return la map des chemins
	 * calcule la distance des cases par rapport à l'entité en param et le met dans une matrice
	 */
	public static int[][] pathfinding(Entity player) {
		int range =player.getRange();
		int nbLine = player.getMap().getNbLine();
		int nbCol = player.getMap().getNbCol();
		int[][] mapChemin = new int[nbLine][nbCol];
		for (int i = 0; i < nbLine; i++) {
			for (int j = 0; j < nbCol; j++) {
				mapChemin[i][j] = -1;
			}
		}
		int valLine = player.getPosLine();
		int valCol = player.getPosCol();
		mapChemin[valLine][valCol]=0;
		
		mapChemin=parcours(player, valLine, valCol, mapChemin, nbLine, nbCol, range);
		return mapChemin;
	}
	
	/**
	 * parcours
	 * 
	 * @param prend une enité,les coordonnées du'en case, une matrice d'entier,les limites de la map nbCol et nbLine et une range
	 * @return la map des chemins
	 * calcule la distance des cases par rapport à l'entité en param et le met dans une matrice fait un appel récursif
	 */
	private static int[][] parcours(Entity player, int valLine, int valCol, int[][] mapChemin, int nbLine, int nbCol,
			int range) {
		if (range == 0) {
			return mapChemin;
		} else {
			if (player.getMap().getCaseAt(MathUtils.realModulo(valLine - 1, nbLine), MathUtils.realModulo(valCol, nbCol))
					.getTypeCase() != TypeCase.WALL) {
				if ((mapChemin[MathUtils.realModulo(valLine - 1, nbLine)][MathUtils.realModulo(valCol, nbCol)] < 0) || mapChemin[valLine][valCol]
						+ 1 < mapChemin[MathUtils.realModulo(valLine - 1, nbLine)][MathUtils.realModulo(valCol, nbCol)]) {
					mapChemin[MathUtils.realModulo(valLine - 1, nbLine)][MathUtils.realModulo(valCol, nbCol)] = mapChemin[valLine][valCol] + 1;
					mapChemin = parcours(player, MathUtils.realModulo(valLine - 1, nbLine), MathUtils.realModulo(valCol, nbCol), mapChemin, nbLine,
							nbCol, range - 1);
				}
			}
			if (player.getMap().getCaseAt(MathUtils.realModulo(valLine, nbLine), MathUtils.realModulo(valCol-1, nbCol))
					.getTypeCase() != TypeCase.WALL) {
				if ((mapChemin[MathUtils.realModulo(valLine, nbLine)][MathUtils.realModulo(valCol - 1, nbCol)] < 0) || mapChemin[valLine][valCol]
						+ 1 < mapChemin[MathUtils.realModulo(valLine, nbLine)][MathUtils.realModulo(valCol - 1, nbCol)]) {
					mapChemin[MathUtils.realModulo(valLine, nbLine)][MathUtils.realModulo(valCol - 1, nbCol)] = mapChemin[valLine][valCol] + 1;
					mapChemin = parcours(player, MathUtils.realModulo(valLine, nbLine), MathUtils.realModulo(valCol - 1, nbCol), mapChemin, nbLine,
							nbCol, range - 1);
				}
			}
			if (player.getMap().getCaseAt(MathUtils.realModulo(valLine + 1, nbLine), MathUtils.realModulo(valCol, nbCol))
					.getTypeCase() != TypeCase.WALL) {
				if ((mapChemin[MathUtils.realModulo(valLine + 1, nbLine)][MathUtils.realModulo(valCol, nbCol)] < 0) || mapChemin[valLine][valCol]
						+ 1 < mapChemin[MathUtils.realModulo(valLine + 1, nbLine)][MathUtils.realModulo(valCol, nbCol)]) {
					mapChemin[MathUtils.realModulo(valLine + 1, nbLine)][MathUtils.realModulo(valCol, nbCol)] = mapChemin[valLine][valCol] + 1;
					mapChemin = parcours(player, MathUtils.realModulo(valLine + 1, nbLine), MathUtils.realModulo(valCol, nbCol), mapChemin, nbLine,
							nbCol, range - 1);
				}
			}
			if (player.getMap().getCaseAt(MathUtils.realModulo(valLine, nbLine), MathUtils.realModulo(valCol + 1, nbCol))
					.getTypeCase() != TypeCase.WALL) {
				if ((mapChemin[MathUtils.realModulo(valLine, nbLine)][MathUtils.realModulo(valCol + 1, nbCol)] < 0) || mapChemin[valLine][valCol]
						+ 1 < mapChemin[MathUtils.realModulo(valLine, nbLine)][MathUtils.realModulo(valCol + 1, nbCol)]) {
					mapChemin[MathUtils.realModulo(valLine, nbLine)][MathUtils.realModulo(valCol + 1, nbCol)] = mapChemin[valLine][valCol] + 1;
					mapChemin = parcours(player, MathUtils.realModulo(valLine, nbLine), MathUtils.realModulo(valCol +1, nbCol), mapChemin, nbLine,
							nbCol, range - 1);
				}
			}
			

		}
		return mapChemin;
	}
	
	/**
	 * wayEntity
	 * 
	 * @param prend une enité, une matrice d'entier
	 * @return un String en direction
	 * donne le côté pour se rapprocher du 0 de la map
	 */
	public static String wayEntity(Entity enemy,int[][] mapChemin ) {
		int valLine= enemy.getPosLine();
		int valCol= enemy.getPosCol();
		int nbLine=enemy.getMap().getNbLine();
		int nbCol=enemy.getMap().getNbCol();
		int val=mapChemin[MathUtils.realModulo(valLine, nbLine)][MathUtils.realModulo(valCol, nbCol)]; // donne la valeur de la case
		if(mapChemin[MathUtils.realModulo(valLine-1, nbLine)][MathUtils.realModulo(valCol, nbCol)]>0&&mapChemin[MathUtils.realModulo(valLine-1, nbLine)][MathUtils.realModulo(valCol, nbCol)]<val) {
			// vérifie si la case est bien compris dans la range de l'autre entité et qu'elle soit plus faible que ça case actuelle
			if(mapChemin[MathUtils.realModulo(valLine-1, nbLine)][MathUtils.realModulo(valCol, nbCol)]<=enemy.getRange()) {
				return "N";
			}
		}
		if(mapChemin[MathUtils.realModulo(valLine, nbLine)][MathUtils.realModulo(valCol-1, nbCol)]>0&&mapChemin[MathUtils.realModulo(valLine, nbLine)][MathUtils.realModulo(valCol - 1, nbCol)]<val) {
			if(mapChemin[MathUtils.realModulo(valLine, nbLine)][MathUtils.realModulo(valCol-1, nbCol)]<=enemy.getRange()) {
			return "W";
			}
		}
		if(mapChemin[MathUtils.realModulo(valLine+1, nbLine)][MathUtils.realModulo(valCol, nbCol)]>0&&mapChemin[MathUtils.realModulo(valLine + 1, nbLine)][MathUtils.realModulo(valCol, nbCol)]<val) {
			if(mapChemin[MathUtils.realModulo(valLine+1, nbLine)][MathUtils.realModulo(valCol, nbCol)]<=enemy.getRange()) {
				return "S";
			}
		}
		if(mapChemin[MathUtils.realModulo(valLine, nbLine)][MathUtils.realModulo(valCol+1, nbCol)]>0&&mapChemin[MathUtils.realModulo(valLine, nbLine)][MathUtils.realModulo(valCol + 1, nbCol)]<val) {
			if(mapChemin[MathUtils.realModulo(valLine, nbLine)][MathUtils.realModulo(valCol+1, nbCol)]<=enemy.getRange()) {
				return "E";
			}
		}
		return "H";
	}

}
