package utils;

import java.io.IOException;

import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

public class Miscellaneous {
	
	/**
	 * Retourne une ArrayList de String des noms de fichiers parsés dans un sous-arbre donné.
	 * Trie les fichiers selon un unique format de fichier (.png, .ogg, etc.)
	 * 
	 * @param source
	 * @param format
	 * @return
	 */
	public static ArrayList<String> parseFolder(String source, String format) {
		DirectoryStream.Filter<Path> filter = file -> {
			return file.toString().endsWith(format) || !file.toString().contains(".");
		};
		ArrayList<String> retour = new ArrayList<String>();
		Stack<DirectoryStream<Path>> subfolders = new Stack<DirectoryStream<Path>>();
		var dirName = Paths.get(source);
		try (var paths = Files.newDirectoryStream(dirName, filter)) {
			subfolders.add(paths);
			while (!subfolders.isEmpty()) {
				Iterator<Path> it = subfolders.pop().iterator();
				while (it.hasNext()) {
					Path current = it.next();
					if (current.toString().endsWith(format)) {
						retour.add(current.toString());
					}
					else {
						subfolders.add(Files.newDirectoryStream(current, filter));
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return retour;
	}
}
