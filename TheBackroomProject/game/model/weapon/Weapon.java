package model.weapon;

import java.util.ArrayList;

import model.Map;
import model.entity.Entity;
import model.entity.TypeEntity;

public class Weapon {
	public static final int MODE_MAGNUM = 0;
	public static final int MODE_SHOOTGUN = 1;
	private int mode;
	private int counterMode0;
	private int counterMode1;
	
	private int timeMode0=1;
	private int timeMode1=5;

	/**
	 * Initialisation de l'arme en pistolet
	 * CounterMode0 et CounterMode1 servent à régler la candence de tir
	 */
	public Weapon() {
		this.mode=0;
		this.counterMode0=0;
		this.counterMode1=0;
	}
	
	/**
	 * Changement du mode via un savant calcul du modulo
	 */
	public void changeMode() {
		this.mode=(this.mode+1)%2;
	}
	
	/**
	 * Crée une ArrayList de projectiles sur laquelle on va pouvoir itérer pour stocker toutes nos bullets
	 * @param e
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param map
	 * @param nbLine
	 * @param nbCol
	 * @return
	 */
	public ArrayList<Projectile> shot(Entity e, int x, int y,int width,int height, Map map, int nbLine,int nbCol){
		double angle;
		float sin;
		float cos;
		int viewportnbCol=nbCol*2+1;
		int viewportnbLine=nbLine*2+1;
		int sizecaseX =width/viewportnbCol;
		int sizecaseY =height/viewportnbLine;
		int dx=(int)(sizecaseX*((float)viewportnbCol/2.0))-x;
		int dy=(int)(sizecaseY*((float)viewportnbLine/2.0))-y;
		
		double hypothenuse = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
		cos = (float) (-1 * dx / hypothenuse);
		sin = (float) (-1* dy / hypothenuse);
		angle = Math.acos(cos);
		if (sin > 0) {
			angle = angle * -1;
		}
		ArrayList<Projectile> listProjectile= new ArrayList<>();
		switch(mode) {
		case MODE_MAGNUM:
			if(this.counterMode0==0&&e.getTypeEntity()==TypeEntity.JFUSION) {
				this.counterMode0++;
				listProjectile.add(new Projectile(e,cos,sin,(float)angle,width,height,map,nbLine,nbCol));
			}
			break;
		case MODE_SHOOTGUN:
			if(this.counterMode1==0&&e.getTypeEntity()==TypeEntity.JFUSION) {
				this.counterMode1++;
				angle=angle;
				listProjectile.add(new Projectile(e,(float)Math.cos(angle),(float)Math.sin(angle),(float)angle,width,height,map,nbLine,nbCol));
				angle=(float)(angle+0.1);
				listProjectile.add(new Projectile(e,(float)Math.cos(angle),(float)Math.sin(angle),(float)angle,width,height,map,nbLine,nbCol));
				angle=(float)(angle-0.2);
				listProjectile.add(new Projectile(e,(float)Math.cos(angle),(float)Math.sin(angle),(float)angle,width,height,map,nbLine,nbCol));
			}
			break;
		}
		
		return listProjectile;
	}
	
	/**
	 * Gestion de la cadence de tir
	 */
	public void tick() {
		if(counterMode0!=0) {
			this.counterMode0=(this.counterMode0+1)%this.timeMode0;
		}
		if(counterMode1!=0) {
			this.counterMode1=(this.counterMode1+1)%this.timeMode1;
		}
	}
	
	/**
	 * 
	 * @return le mode d'arme actuel
	 */
	public int getMode() {
		return this.mode;
	}
}
