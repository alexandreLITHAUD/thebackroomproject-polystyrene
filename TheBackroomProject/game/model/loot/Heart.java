package model.loot;

import model.Map;

public class Heart extends Loot{

	//set un loot heart dans la map
	private int life;
	
	public Heart(int l, int c, Map m) {
		super(l, c, m);
		this.life = 250;		// Stat pour les butins de vie
	}
	
	/**
	 * Cette fonction retourne la valeur de la variable vie (life)
	 * pour les butins.
	 * 
	 * @return La variable life est renvoyée.
	 */
	public int getLife() {
		return this.life;
	}

	@Override
	/**
	 * Cette méthode renvoie le type de butin 'vie'.
	 * 
	 * @return entier type vie (liste des types dans TypeLoot)
	 * @see TypeLoot
	 */
	public int getTypeLoot() {
		return TypeLoot.HEART;
	}

}
