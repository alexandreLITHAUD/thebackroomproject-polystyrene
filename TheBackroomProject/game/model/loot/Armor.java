package model.loot;

import model.Map;

public class Armor extends Loot{
	
	//set un loot armor dans la map
	private int armor;

	public Armor(int l, int c, Map m) {
		super(l, c, m);
		this.armor = 50;		// Stat pour les butins d'armure
	}
	
	/**
	 * Cette fonction renvoie la valeur de la variable d'armure (armor)
	 * pour les butins.
	 * 
	 * @return La variable d'armure est renvoyée.
	 */
	public int getArmor() {
		return armor;
	}

	@Override
	/**
	 * Cette méthode renvoie le type de butin 'armure'.
	 * 
	 * @return entier type armure (liste des types dans TypeLoot)
	 * @see TypeLoot
	 */
	public int getTypeLoot() {
		return TypeLoot.ARMOR;
	}
}
