package model.loot;

import model.GameItem;
import model.Map;

public abstract class Loot extends GameItem{

	public Loot(int l, int c, Map m) {
		super(l, c, m);
	}
	
	/**
	 * Cette méthode abstraite permet de renvoyer le type de butin.
	 * 
	 * @return un entier qui définit un type de butin (loot)
	 * @see TypeLoot
	 */
	public abstract int getTypeLoot();
}
