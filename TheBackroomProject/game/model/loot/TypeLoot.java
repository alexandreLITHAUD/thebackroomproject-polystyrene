package model.loot;

/**
 * TypeLoot : Description des différents loots du jeu
 */
public class TypeLoot {
	public static final int FOOD = 0;
	public static final int HEART = 1;
	public static final int AMMO = 2;
	public static final int ARMOR = 3;

}
