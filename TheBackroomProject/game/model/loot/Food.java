package model.loot;

import model.Map;

public class Food extends Loot{
	
	//set un loot food dans la map
	private int food;
	
	public Food(int l, int c, Map m) {
		super(l, c, m);
		this.food = 50;		// Stat pour les butins d'alimentations
	}
	
	/**
	 * Cette fonction renvoie la valeur de la variable nourriture (food)
	 * pour les butins.
	 * 
	 * @return La variable food est renvoyée.
	 */
	public int getFood() {
		return this.food;
	}

	@Override
	/**
	 * Cette méthode renvoie le type de butin 'nourriture'.
	 * 
	 * @return entier type nourriture (liste des types dans TypeLoot)
	 * @see TypeLoot
	 */
	public int getTypeLoot() {
		return TypeLoot.FOOD;
	}

}
