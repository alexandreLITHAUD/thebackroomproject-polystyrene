package model;

import java.util.ArrayList;
import java.util.Random;

import model.entity.Entity;
import model.entity.Player;
import model.entity.Shadow;
import model.entity.TypeEntity;
import model.loot.Loot;

/**
 * Map : Représentation de la map du jeu
 */
public class Map {
	/* dimensions */
	private int nbLine;
	private int nbCol;
	private int[][] mapchemin;
	/* joueurs sur la map */
	private Player j1;

	/* map elle meme */
	private Case[][] map;

	/* générateur pour la création */
	private Random rand;

	/* liste des objets du viewport */
	private ArrayList<Case> viewPort;
	private int centerLine;
	private int centerCol;
	private int viewPortSizeCol;
	private int viewPortSizeLine;
	private int vpRayCol;
	private int vpRayLine;
	private int difficulty;
	
	private boolean isOnExit;

	/**
	 * Création d'une map avec l lignes et c colonnes et une seed random
	 * 
	 * @param l Ligne de la case
	 * @param c Colonne de la case
	 */
	public Map(int l, int c, int d) {
		this.nbCol = c;
		this.nbLine = l;
		this.difficulty = d;
		this.rand = new Random();

		init();
	}

	/**
	 * Création d'une map avec l lignes et c colonnes et une seed fixée
	 * 
	 * @param l    Ligne de la case
	 * @param c    Colonne de la case
	 * @param seed Seed de génération de la map
	 */
	public Map(int l, int c, int d, int seed) {
		this.nbCol = c;
		this.nbLine = l;
		this.difficulty = d;
		this.rand = new Random(seed);

		init();
	}

	/**
	 * Donne la case se trouvant a la position donnée
	 * 
	 * @return Case a la position l,c
	 * @param l Ligne de la case demandé
	 * @param c Colonne de la case demandé
	 */
	public Case getCaseAt(int l, int c) {
		return this.map[l][c];
	}

	/**
	 * Utilise une map factory pour creer la map (le tableau de case)
	 */
	private void init() {
		this.j1 = new Player(0, 0, this);
		/* Init du viewport */
		this.viewPort = new ArrayList<>();
		this.centerLine = -1;
		this.centerCol = -1;
		this.viewPortSizeLine = -1;
		this.viewPortSizeCol = -1;
		this.vpRayCol = -1;
		this.vpRayLine = -1;
		
		
		this.isOnExit = false;
		/* Map random utile pour les debugs */
		//this.map = MapFactoryRand.initMap(this.nbLine, this.nbCol, this.j1, this.j2, this.rand);
		/* vrai labyrinthe */
		//this.map = MapFactoryMaze.initMap(this.nbLine, this.nbCol, this.j1, this.j2, this.rand);
		this.map = utils.MapFactoryMazeV2.initMap(this.nbLine, this.nbCol,
				this.difficulty, this.j1, this.rand);
	}

	/**
	 * Donne le joueur 1
	 * 
	 * @return le joueur 1
	 */
	public Player getJ1() {
		return j1;
	}

	/**
	 * Donne le nombre de ligne
	 * 
	 * @return le nombre de ligne de la map
	 */
	public int getNbLine() {
		return nbLine;
	}

	/**
	 * Donne le nombre de colonnes
	 * 
	 * @return le nombre de colonnes de la map
	 */
	public int getNbCol() {
		return nbCol;
	}
	
	public void setMapChemin() {
		this.mapchemin=utils.MathUtils.pathfinding(j1);
	}
	
	public int[][] getMapChemin(){
		return this.mapchemin;
	}

	/**
	 * Fonction qui déplace une entité d'une case a une autre. Cette fonction ne
	 * fait pas les vérifiactions sur la possibilité de ce déplacement
	 * 
	 * @param j  Entité a déplacer
	 * @param tL Ligne destination
	 * @param tC Colonne destination
	 */
	private void moveEntity(Entity j, int tL, int tC) {
		//System.out.println("Move de "+j.line+" "+ j.column + " vers "+ tL + " "+tC);
		
		/* Case de départ */
		Case c = this.map[j.getPosLine()][j.getPosCol()];

		/* récupération et suppression du gameItem de cette case */
		// TODO pourquoi on fait pas avec le joueur direct
		GameItem gi = c.getGameItem();
		c.removeGameItem();

		/* ajout de l'entité a l'arrivée */
		Case d = this.map[tL][tC];
		d.setGameItem(gi);

		/* on informe l'entité qu'elle a bien bougé */
		j.move(tL, tC);
		
		/* verification de la sortie */
		if((j.getTypeEntity() == TypeEntity.JFUSION || j.getTypeEntity() == TypeEntity.PLAYER1) && d.getTypeCase() == TypeCase.PORTAL) {
			this.isOnExit = true;
		}
	}
	
	/**
	 * Renvoie vrai si l'un des joueurs est sur la sortie 
	 * 
	 * @return true si un joueur est sur l'arrivée
	 */
	public boolean getIsOnExit() {
		return isOnExit;
	}
	
	public boolean isInViewPort(int l,int c) {
		return this.viewPort.contains(getCaseAt(l, c));
	}
	
	/**
	 * Vérifie la présentce de GameItem sur la case donner en paramètre
	 * si oui: adapte son comportement en se détruisant ou en collectant un objet
	 * sinon se déplace
	 * 
	 * @param s
	 * @param l
	 * @param c
	 * @return boolean
	 */
	private boolean checkShadow(Shadow s, int l, int c) {
		if(map[l][c].hasGameItem()) {
			if(map[l][c].getGameItem() instanceof Loot) {
				s.getPlayer().collectLoot(map[l][c]);
				moveEntity(s, l, c);
				return true;
			}else {
				Entity entity = (Entity) map[l][c].getGameItem();
				if(entity.getTypeEntity() == TypeEntity.PLAYER1) {
					s.destroy();
					return true;
				}else {
					s.destroy();
					s.getPlayer().createPossession(entity);
					return true;
				}
			}
		}else {
			if(!isInViewPort(l, c)) {
				s.destroy();
				return false;
			}
			moveEntity(s, l, c);
			return true;
		}
	}

	/**
	 * Déplacement d'une case vers le nord de l'entité
	 * 
	 * @param j Entité a déplacer
	 */
	public boolean moveUp(Entity j) {
		int l = j.getPosLine();
		int c = j.getPosCol();
		/* calcul de la ligne d'arrivée */
		int x = utils.MathUtils.realModulo(l - 1, this.nbLine);
		
		/* Déplacement de l'ombre se fait peu importe la case ! */
		if(j.getTypeEntity() == TypeEntity.OMBRE) {
			return checkShadow((Shadow)j,x,c);
		}
		
		/* check de la sortie de la possession */
		if(j1.getState() == Player.possession && j1.getPossessEntity()==j && !isInViewPort(x, c)) {
			j1.destroyPossession();
			return false;
		}
		
		/* verification de la possibilitée du déplacement */
		if (map[x][c].getIsCrossable() && !map[x][c].hasGameItem()) {
			moveEntity(j, x, c);
			return true;
		}
		// SI ON VEUX QUE CA RENVOIE FAUX QUAND MOVE MARCHE PAS IL FAUT RAJOUTER UNE EXCEPTION ICI
		return false;
	}

	/**
	 * Déplacement d'une case vers le sud de l'entité
	 * 
	 * @param j Entité a déplacer
	 */
	public boolean moveDown(Entity j) {
		int l = j.getPosLine();
		int c = j.getPosCol();
		/* calcul de la ligne d'arrivée */
		int x = utils.MathUtils.realModulo(l + 1, this.nbLine);
		
		/* Déplacement de l'ombre se fait peu importe la case ! */
		if(j.getTypeEntity() == TypeEntity.OMBRE) {
			return checkShadow((Shadow)j,x,c);
		}
		
		/* check de la sortie de la possession */
		if(j1.getState() == Player.possession && j1.getPossessEntity()==j && !isInViewPort(x, c)) {
			j1.destroyPossession();
			return false;
		}
		
		/* verification de la possibilitée du déplacement */
		if (map[x][c].getIsCrossable() && !map[x][c].hasGameItem()) {
			moveEntity(j, x, c);
			return true;
		}
		// SI ON VEUX QUE CA RENVOIE FAUX QUAND MOVE MARCHE PAS IL FAUT RAJOUTER UNE EXCEPTION ICI
		return false;
	}

	/**
	 * Déplacement d'une case vers l'est de l'entité
	 * 
	 * @param j Entité a déplacer
	 */
	public boolean moveRight(Entity j) {
		int l = j.getPosLine();
		int c = j.getPosCol();
		/* calcul de la colonne d'arrivée */
		int x = utils.MathUtils.realModulo(c + 1, this.nbCol);
		
		/* Déplacement de l'ombre se fait peu importe la case ! */
		if(j.getTypeEntity() == TypeEntity.OMBRE) {
			return checkShadow((Shadow)j,l,x);
		}
		
		/* check de la sortie de la possession */
		if(j1.getState() == Player.possession && j1.getPossessEntity()==j && !isInViewPort(l, x)) {
			j1.destroyPossession();
			return false;
		}
		
		/* verification de la possibilitée du déplacement */
		if (map[l][x].getIsCrossable() && !map[l][x].hasGameItem()) {
			moveEntity(j, l, x);
			return true;
		}
		// SI ON VEUX QUE CA RENVOIE FAUX QUAND MOVE MARCHE PAS IL FAUT RAJOUTER UNE EXCEPTION ICI
		return false;
	}

	/**
	 * Déplacement d'une case vers l'ouest de l'entité
	 * 
	 * @param j Entité a déplacer
	 */
	public boolean moveLeft(Entity j) {
		int l = j.getPosLine();
		int c = j.getPosCol();
		/* calcul de la colonne d'arrivée */
		int x = utils.MathUtils.realModulo(c - 1, this.nbCol);
		
		
		/* Déplacement de l'ombre se fait peu importe la case ! */
		if(j.getTypeEntity() == TypeEntity.OMBRE) {
			return checkShadow((Shadow)j,l,x);
		}
		
		/* check de la sortie de la possession */
		if(j1.getState() == Player.possession && j1.getPossessEntity()==j && !isInViewPort(l, x)) {
			j1.destroyPossession();
			return false;
		}
		
		/* verification de la possibilitée du déplacement */
		if (map[l][x].getIsCrossable() && !map[l][x].hasGameItem()) {
			moveEntity(j, l, x);
			return true;
		}
		// SI ON VEUX QUE CA RENVOIE FAUX QUAND MOVE MARCHE PAS IL FAUT RAJOUTER UNE EXCEPTION ICI
		return false;
	}

	/**
	 * Vérifie une case vers le nord de l'entité si un adversaire
	 * s'y trouve.
	 * 
	 * @param j Entité a attaqué
	 */
	public boolean hitUp(Entity j) {
		int l = j.getPosLine();
		int c = j.getPosCol();
		/* calcul de la ligne d'arrivée */
		int x = utils.MathUtils.realModulo(l - 1, this.nbLine);
		/* verification de la possibilitée du déplacement */
		if (map[x][c].hasGameItem() && map[x][c].getGameItem() instanceof Entity) {		//(((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.HOUND || ((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.SLIME || ((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.ZOMBIE)
			
			Entity i = (Entity) map[x][c].getGameItem();
			i.isHit(j);
			i.isDead();
			return true;
		}
		// SI ON VEUX QUE CA RENVOIE FAUX QUAND MOVE MARCHE PAS IL FAUT RAJOUTER UNE EXCEPTION ICI
		return false;
	}

	/**
	 * Vérifie une case vers l'ouest de l'entité si un adversaire
	 * s'y trouve.
	 * 
	 * @param j Entité a attaqué
	 */
	public boolean hitLeft(Entity j) {
		int l = j.getPosLine();
		int c = j.getPosCol();
		/* calcul de la ligne d'arrivée */
		int x = utils.MathUtils.realModulo(c - 1, this.nbLine);
		/* verification de la possibilitée du déplacement */
		if (map[l][x].hasGameItem() && map[l][x].getGameItem() instanceof Entity) {		//(((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.HOUND || ((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.SLIME || ((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.ZOMBIE)
			
			Entity i = (Entity) map[l][x].getGameItem();
			i.isHit(j);
			i.isDead();
			return true;
		}
		// SI ON VEUX QUE CA RENVOIE FAUX QUAND MOVE MARCHE PAS IL FAUT RAJOUTER UNE EXCEPTION ICI
		return false;
	}

	/**
	 * Vérifie une case vers l'est de l'entité si un adversaire
	 * s'y trouve.
	 * 
	 * @param j Entité a attaqué
	 */
	public boolean hitRight(Entity j) {
		int l = j.getPosLine();
		int c = j.getPosCol();
		/* calcul de la ligne d'arrivée */
		int x = utils.MathUtils.realModulo(c + 1, this.nbLine);
		/* verification de la possibilitée du déplacement */
		
		//TODO FAIRE ATTENTION AU DEUXIEME JOUEUR
		if (map[l][x].hasGameItem() && map[l][x].getGameItem() instanceof Entity) {		//(((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.HOUND || ((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.SLIME || ((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.ZOMBIE)
			
			Entity i = (Entity) map[l][x].getGameItem();
			i.isHit(j);
			i.isDead();
			return true;
		}
		// SI ON VEUX QUE CA RENVOIE FAUX QUAND MOVE MARCHE PAS IL FAUT RAJOUTER UNE EXCEPTION ICI
		return false;
	}
	
	public boolean hitHere(Entity j) {
		int l = j.getPosLine();
		int c = j.getPosCol();
		/* verification de la possibilitée du déplacement */
		
		//TODO FAIRE ATTENTION AU DEUXIEME JOUEUR
		if (map[l][c].hasGameItem() && map[l][c].getGameItem() instanceof Entity) {		//(((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.HOUND || ((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.SLIME || ((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.ZOMBIE)
			
			Entity i = (Entity) map[l][c].getGameItem();
			i.isHit(j);
			i.isDead();
			return true;
		}
		// SI ON VEUX QUE CA RENVOIE FAUX QUAND MOVE MARCHE PAS IL FAUT RAJOUTER UNE EXCEPTION ICI
		return false;
	}

	/**
	 * Vérifie une case vers le sud de l'entité si un adversaire
	 * s'y trouve.
	 * 
	 * @param j Entité a attaqué
	 */
	public boolean hitDown(Entity j) {
		int l = j.getPosLine();
		int c = j.getPosCol();
		/* calcul de la ligne d'arrivée */
		int x = utils.MathUtils.realModulo(l + 1, this.nbLine);
		/* verification de la possibilitée du déplacement */
		if (map[x][c].hasGameItem() && map[x][c].getGameItem() instanceof Entity) {		//(((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.HOUND || ((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.SLIME || ((Entity)map[l][x].getGameItem()).getTypeEntity() == TypeEntity.ZOMBIE)	
			Entity i = (Entity) map[x][c].getGameItem();
			i.isHit(j);
			i.isDead();
			return true;
		}
		// SI ON VEUX QUE CA RENVOIE FAUX QUAND MOVE MARCHE PAS IL FAUT RAJOUTER UNE EXCEPTION ICI
		return false;
	}


	/**
	 * Calcul la liste des cases faisant parti du viewport
	 * 
	 * @param rl rayon en ligne du viewport
	 * @param rc rayon en colonne du viewport
	 * @param j  joueur au centre du viewport
	 */
	public ArrayList<Case> getViewPortCoord(int rl, int rc, Player j) {
		/* si le viewport a bougé */
		if (rl != this.vpRayLine || rc != this.vpRayCol || j.getPosLine() != this.centerLine
				|| j.getPosCol() != this.centerCol) {
			calculViewPortCoord(rl, rc, j);
			this.vpRayLine = rl;
			this.vpRayCol = rc;
		}
		return viewPort;
	}

	/**
	 * Met a jour le viewport
	 * 
	 * @param rl rayon en ligne du viewport
	 * @param rc rayon en colonne du viewport
	 * @param j  joueur au centre du viewport
	 */
	public void calculViewPortCoord(int rl, int rc, Player j) {
		/* calcul de la taille du viewport */
		this.viewPortSizeLine = rl * 2 + 1;
		this.viewPortSizeCol = rc * 2 + 1;
		if (this.viewPortSizeLine >= this.nbLine) {
			this.viewPortSizeLine = this.nbLine;
		}
		if (this.viewPortSizeCol >= this.nbCol) {
			this.viewPortSizeCol = this.nbCol;
		}
		ArrayList<Case> liste = new ArrayList<>();

		this.centerLine = j.getPosLine();
		this.centerCol = j.getPosCol();

		/* calcul de la case du haut a gauche */
		int hautL = utils.MathUtils.realModulo(this.centerLine - (rl+1), nbLine);
		int hautC = utils.MathUtils.realModulo(this.centerCol - (rc+1), nbCol);

		/* calcul de toutes les autres cases */
		for (int i = 0; i < this.viewPortSizeLine+2; i++) {
			for (int k = 0; k < this.viewPortSizeCol+2; k++) {
				liste.add(map[(hautL + i) % this.nbLine][(hautC + k) % this.nbCol]);
			}
		}
		/* MAJ des attributs */
		this.viewPort = liste;
	}

	/**
	 * Donne la taille du vp
	 * 
	 * @return La taille du viewPort en ligne
	 */
	public int getViewPortSizeLine() {
		return this.viewPortSizeLine;
	}

	/**
	 * Donne la taille du vp
	 * 
	 * @return La taille du viewPort en colonne
	 */
	public int getViewPortSizeCol() {
		return this.viewPortSizeCol;
	}

	/**
	 * Imprime la map dans le termianl
	 */
	public void printMap() {
		for (int i = 0; i < this.nbLine; i++) {
			for (int j = 0; j < this.nbCol; j++) {
				switch (map[i][j].getTypeCase()) {
				case TypeCase.WALL:
					System.out.print('W');
					break;
				case TypeCase.NORMALGROUND:
				case TypeCase.WATERGROUND:
				case TypeCase.FIREGROUND:
					if (map[i][j].hasGameItem() && map[i][j].getGameItem() instanceof Player) {
						System.out.print('J');
					} else {
						System.out.print('O');
					}
					break;
				case TypeCase.PORTAL:
					System.out.print('P');
					break;
				}
			}
			System.out.println(' ');
		}
	}

}
