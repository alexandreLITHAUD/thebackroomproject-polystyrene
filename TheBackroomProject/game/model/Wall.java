package model;

/**
 * Wall : Représentation du mur de la map
 */
public class Wall extends Case {

	/**
	 * Création d'une case mur
	 * 
	 * @param l Ligne de la case
	 * @param c Colonne de la case
	 */
	public Wall(int l, int c) {
		super(l, c);
		isCrossable = false;
	}

	/**
	 * Donne le type d'une case mur
	 * 
	 * @return entier décrivant le type mur (liste des types dans TypeCase)
	 * @see TypeCase
	 */
	@Override
	public int getTypeCase() {
		return TypeCase.WALL;
	}

}
