package model;

/**
 * Case : Représentation abstraite d'un gameItem, cela représentera les joueurs,
 * monstres, items, etc
 */
public abstract class GameItem {
	/* position */
	protected int line;
	protected int column;
	protected Map map;

	/**
	 * Création d'un gameItem
	 * 
	 * @param l Ligne de la case
	 * @param c Colonne de la case
	 * @param m Map de l'item
	 */
	public GameItem(int l, int c, Map m) {
		this.line = l;
		this.column = c;
		this.map = m;
	}

	/**
	 * Retourne la ligne de l'item
	 * 
	 * @return ligne de l'item
	 */
	public int getPosLine() {
		return this.line;
	}

	/**
	 * Retourne la colonne de l'item
	 * 
	 * @return colonne de l'item
	 */
	public int getPosCol() {
		return this.column;
	}
	
	/**
	 * Retourne la map de l'item
	 * 
	 * @return map de l'item
	 */
	public Map getMap() {
		return this.map;
	}
	
}
