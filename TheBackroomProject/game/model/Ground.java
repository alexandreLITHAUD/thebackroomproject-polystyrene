package model;

/**
 * Ground : Représentation du sol de la map
 */
public abstract class Ground extends Case {
	/**
	 * Création d'une case sol
	 * 
	 * @param l Ligne de la case
	 * @param c Colonne de la case
	 */
	public Ground(int l, int c) {
		super(l, c);
		isCrossable = true;
	}

	/**
	 * Donne le type d'une case sol
	 * 
	 * @return entier décrivant le type sol (liste des types dans TypeCase)
	 * @see TypeCase
	 */
	@Override
	public abstract int getTypeCase();

}
