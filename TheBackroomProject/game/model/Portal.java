package model;

/*
* Wall : Représentation du mur de la map
*/
public class Portal extends Case {

	/**
	 * Création d'une case portail
	 * 
	 * @param l Ligne de la case
	 * @param c Colonne de la case
	 */

	public Portal(int l, int c) {
		super(l, c);
		isCrossable = true;
		// TODO Auto-generated constructor stub
	}

	/**
	 * Donne le type d'une case portail
	 * 
	 * @return entier décrivant le type portail (liste des types dans TypeCase)
	 * @see TypeCase
	 */
	@Override
	public int getTypeCase() {
		// TODO Auto-generated method stub
		return TypeCase.PORTAL;
	}

}
