package model.entity;

import java.awt.Graphics;

import controller.MainController;
import info3.game.Game;
import utils.MathUtils;

import model.Case;
import model.GameItem;
import model.Map;

import model.loot.Armor;

import model.loot.Food;
import model.loot.Heart;
import model.loot.Ammo;

import vue.AvatarPlayer;

/**
 * Player : Représentation de l'entité joueur
 */
public class Player extends Entity {
	
	private AvatarPlayer avatar;
	private LiningPlayer lining;
	
	public void askForPaint(Graphics g, int x, int y, int dx, int dy) {
		avatar.paint(g, x, y, dx, dy);
	}
	
	// Statistique du personnage
	private int maxSatiety;
	private int currentSatiety;
	private int maxArmor;
	private int currentArmor;
	private int maxAmmo;
	private int currentAmmo;
	
	// Gestion de la fusion
	private Shadow shadow;
	private Entity possessEntity;
	public static final int fusion = 0; 
	public static final int split = 1;
	public static final int possession = 2;
	private int state = fusion;
	
	//santé mentale
	private int maxMentalHealth = 1200;
	private int currentMentalHealth = maxMentalHealth;
	private int tickCounter = 0;
	private int tempoMentalHealth = 100;
	
	/**
	 * Création d'un joueur
	 *
	 * @param l Ligne du joueur
	 * @param c Colonne du joueur
	 * @param m Map du joueur
	 */
	public Player(int l, int c, Map m) {
		super(l, c, m);
		this.avatar = new AvatarPlayer(this);
		this.maxHealth = 500;
		this.currentHealth = maxHealth;
		this.maxSatiety = 150;
		this.currentSatiety = maxSatiety;
		this.maxAmmo = 300;
		this.lining = new LiningPlayer(this);
		this.maxArmor = 200;
		switch (Game.difficulty) {
		case Game.EASY:
			this.damages = 25;
			this.currentArmor = this.maxArmor;
			this.currentAmmo = maxAmmo/2;
			break;
		case Game.HARD:
			this.damages = 20;
			this.currentArmor = this.maxArmor;
			this.currentAmmo = maxAmmo/2;
			break;
		case Game.NUKE:
			this.damages = 10;
			this.currentArmor = 0;
			this.currentAmmo = 0;
			break;
		default:
			this.damages = 1;
			this.currentArmor = 0;
			this.currentAmmo = 0;
			break;
		}
	}

	/**
	 * Déplace le joueur aux coordonnées indiqués
	 * 
	 * @param l Ligne destination du joueur
	 * @param c Colonne destination du joueur
	 */
	@Override
	public void move(int l, int c) {
		this.line = l;
		this.column = c;
		if(this.state == split && !this.map.isInViewPort(shadow.getPosLine(), shadow.getPosCol())) {
			destroyShadow();
		}
		if(this.state == possession && !this.map.isInViewPort(possessEntity.getPosLine(), possessEntity.getPosCol())) {
			destroyPossession();
		}
	}


	@Override
	public void pop(String str) {
		
		switch(str) {
		
		case "N":
		case "F":
			this.moveUp();
			return;
		case "E":
		case "R":
			this.moveRight();
			return;
		case "W":
		case "L":
			this.moveLeft();
			return;
		case "S":
		case "B":
			this.moveDown();
			return;
		default:
			return;
		}
		
	}

	@Override
	public void wizz(String str) {
		if(this.state == fusion) {
			this.createShadow();
		}else {
			this.destroyShadow();
		}
	}

	@Override
	public int getRange() {
		
		return 10;
	}
	
	/**
	 * Deplace l'entité vers le Nord
	 */
	@Override
	public boolean moveUp() {
		if (avatar.moving) {
			return false;
		}
		this.avatar.setOrientation("N");
		if(this.map.moveUp(this)) {
			this.avatar.setAnimationSpeed(this.lining.getLining());
			this.avatar.setMoving(true);
		};
		return true;
	}

	/**
	 * Deplace l'entité vers le Sud
	 */
	@Override
	public boolean moveDown() {
		if (avatar.moving) {
			return false;
		}
		this.avatar.setOrientation("S");
		if(this.map.moveDown(this)) {
			this.avatar.setAnimationSpeed(this.lining.getLining());
			this.avatar.setMoving(true);
		};
		return true;
	}

	/**
	 * Deplace l'entité vers l'ouest
	 */
	@Override
	public boolean moveLeft() {
		if (avatar.moving) {
			return false;
		}
		this.avatar.setOrientation("W");
		if(this.map.moveLeft(this)) {
			this.avatar.setAnimationSpeed(this.lining.getLining());
			this.avatar.setMoving(true);
		};
		
		return true;
	}

	/**
	 * Deplace l'entité vers l'est
	 */
	@Override
	public boolean moveRight() {
		if (avatar.moving) {
			return false;
		}
		this.avatar.setOrientation("E");
		if(this.map.moveRight(this)) {
			this.avatar.setAnimationSpeed(this.lining.getLining());
			this.avatar.setMoving(true);
		};
		
		return true;
	}
	
	
	public void tick(long elapsed) {
		this.avatar.tick(elapsed);
		if(this.tickCounter > this.tempoMentalHealth) {
			this.tickCounter = 0;
			if(state == fusion) {
				this.currentMentalHealth = Math.min(currentMentalHealth + 8, maxMentalHealth);
			}else {
				this.currentMentalHealth = Math.max(currentMentalHealth - 1, 0);
			}
		}
		this.tickCounter ++;
	}
	

	public int getTypeEntity() {
		if (state == fusion) {
			return TypeEntity.JFUSION;
		}else {
			return TypeEntity.PLAYER1;
		}
		
	}
	
	public boolean isHit() {
		return false;
	}
	
	public int getMaxSatiety() {
		return this.maxSatiety;
	}
	
	public int getCurrentSatiety() {
		return this.currentSatiety;
	}
	
	public void setCurrentSatiety(int newSatiety) {
		this.currentSatiety = newSatiety;
	}
	
	public int getMaxAmmo() {
		return this.maxAmmo;
	}
	
	public int getCurrentAmmo() {
		return this.currentAmmo;
	}
	
	public void setCurrentAmmo(int newAmmo) {
		this.currentAmmo = newAmmo;
	}

	@Override
	public int getMaxHeath() {
		
		return this.maxHealth;
	}

	@Override
	public void setCurrentHealth(int newHealth) {
		
		this.currentHealth = newHealth;
	}

	@Override
	public int getCurrentHealth() {
		
		return this.currentHealth;
	}

	@Override
	public boolean isDead() {
		if(this.currentHealth <= 0) {
			this.deadEntity();
			return true;
		}
		return false;
	}

	// TODO SUS
	@Override
	public void deadEntity() {
		this.getAutomata().getController().getGame().addRemoveEntities(this);
	}

	@Override
	public int hit() {
		
		return this.damages;
	}
	
	
	/**
	 * récupère le loot, et le détruit des liste et de la case
	 * applique aussi l'effet du loot
	 * 
	 * @return un booleen si il a executer l'action
	 */
	public boolean pickUp() {
		if (avatar.moving) {
			return false;
		}
		Case c=this.map.getCaseAt( MathUtils.realModulo(this.getPosLine()-1,this.map.getNbLine()), this.getPosCol());
		if(c.hasGameItem()) {
			this.collectLoot(c);
		}
		return true;
	}
	
	
	/**
	 * récupère le loot, et le détruit des liste et de la case
	 * applique aussi l'effet du loot
	 * 
	 * @return un booleen si il a executer l'action
	 */
	public boolean pickRight() {
		if (avatar.moving) {
			return false;
		}
		Case c=this.map.getCaseAt(this.getPosLine(),  MathUtils.realModulo(this.getPosCol()+1,this.map.getNbCol()));
		if(c.hasGameItem()) {
			this.collectLoot(c);
		}
		return true;
	}
	
	/**
	 * récupère le loot, et le détruit des liste et de la case
	 * applique aussi l'effet du loot
	 * 
	 * @return un booleen si il a executer l'action
	 */
	public boolean pickLeft() {
		if (avatar.moving) {
			return false;
		}
		Case c=this.map.getCaseAt(this.getPosLine(), MathUtils.realModulo(this.getPosCol()-1,this.map.getNbCol()));
		if(c.hasGameItem()) {
			this.collectLoot(c);
		}
		return true;
	}
	
	/**
	 * récupère le loot, et le détruit des liste et de la case
	 * applique aussi l'effet du loot
	 * 
	 * @return un booleen si il a executer l'action
	 */
	public boolean pickDown() {
		if (avatar.moving) {
			return false;
		}
		Case c=this.map.getCaseAt(MathUtils.realModulo(this.getPosLine()+1,this.map.getNbLine()), this.getPosCol());
		if(c.hasGameItem()) {
			this.collectLoot(c);
		}
		return true;
	}
	
	/**
	 * Fonction qui recupere les Loot de la case donnée en parametre
	 * @param c La case avec le loot
	 */
	public void collectLoot(Case c) {
		GameItem game=c.getGameItem();
		if(game instanceof Food) {
			this.currentSatiety=Math.min(this.maxSatiety, this.currentSatiety+((Food)game).getFood());
		}
		if(game instanceof Heart) {
			this.currentHealth=Math.min(this.maxHealth, this.currentHealth+((Heart)game).getLife());
		}
		if(game instanceof Armor) {
			this.currentArmor=Math.min(this.maxArmor, this.currentArmor+((Armor)game).getArmor());
		}
		if(game instanceof Ammo) {
			this.currentAmmo=Math.min(this.maxAmmo, this.currentAmmo+((Ammo)game).getAmmo());
		}
		c.setGameItem(null);
		this.getAutomata().getController().getGame().getLoot().remove(game);
	}
	

	@Override
	public int isHit(Entity e) {
		if(this.state == fusion) {
			createPossession(e);
		}
		if(this.currentArmor >= e.damages) {
			return this.currentArmor -= e.damages;
		}
		if(this.currentArmor > 0 && this.currentArmor < e.damages) {
			int temp = e.damages - this.currentArmor;
			this.currentArmor = 0;
			this.currentHealth -= temp;
		}
		this.avatar.isHit();
		return this.currentHealth -= e.damages;
	}
	
	/**
	 * Fonction qui permet de changer l'état du joueur
	 * et de mettre a jours les automates en fonction
	 */
	public void switchAutomata(){				
		this.getAutomata().getController().getGame().addNewEntities(this.shadow);
		this.state = split;//split
		//changement de l'automate du joueur
		this.getAutomata().getController().setAutomata(this);
	}
	
	/**
	 * Fonction qui creer l'ombre et met a jour toutes les information du joueur
	 */
	public void createShadow() {
		Case c=null;
		switch(avatar.getOrientation()) {
		case AvatarPlayer.EAST:
			c = this.map.getCaseAt(this.getPosLine(), MathUtils.realModulo(this.getPosCol()-1,this.map.getNbCol()));
			break;
		case AvatarPlayer.WEST:
			c = this.map.getCaseAt(this.getPosLine(), MathUtils.realModulo(this.getPosCol()+1,this.map.getNbCol()));
			break;
		case AvatarPlayer.NORTH:
			c = this.map.getCaseAt(MathUtils.realModulo(this.getPosLine()+1, this.map.getNbLine()), this.getPosCol());
			break;
		case AvatarPlayer.SOUTH:
			c = this.map.getCaseAt(MathUtils.realModulo(this.getPosLine()-1, this.map.getNbLine()), this.getPosCol());
			break;
		}
		
		//verif si un enemy sur case spawn
		if(c.hasGameItem() && c.getGameItem() instanceof Entity) {
			createPossession((Entity)c.getGameItem());
			return;
		}
		//mise dans la liste d'entité
		Entity entity = new Shadow(c, map, this);
		this.shadow = (Shadow)entity;
		
		this.getAutomata().getController().getGame().doShadow = true;
	}
	
	/**
	 * Fonction qui detruit l'ombre et met a jour les information du joueur principal
	 */
	public void destroyShadow() {
		if(this.state == split) {
			this.state = fusion;//fusion,
			this.getAutomata().getController().getGame().addRemoveEntity(this.shadow);
			this.shadow = null;
			//changement de l'automate du joueur
			this.getAutomata().getController().setAutomata(this);
		}
	}
	
	/**
	 * Fonction qui met a jours les information pour informer
	 * le jeu qu'une possession a lieux
	 * @param e l'entité a posseder
	 */
	public void createPossession(Entity e) {
		this.possessEntity = e;
		this.getAutomata().getController().getGame().doPossession = true;
	}
	
	/**
	 * Fonction qui met a jours les information pour informer
	 * le jeu qu'une possession a lieux
	 */
	public void createPossession() {
		this.state = possession;
		//changement de l'automate du joueur
		this.getAutomata().getController().setAutomata(this);
		//cht automate entity
		this.possessEntity.getAutomata().getController().setAutomata(this.possessEntity,MainController.ombreAutoName);
	}
	
	/**
	 * Fonction qui detruit la possésion et met a jour toutes les informations
	 */
	public void destroyPossession() {
		if(this.state == possession) {
			this.state = fusion;//fusion,
			this.getAutomata().getController().getGame().addRemoveEntity(this.possessEntity);
			this.possessEntity = null;
			//changement de l'automate du joueur
			this.getAutomata().getController().setAutomata(this);
		}
	}
	
	/**
	 * Fonction qui detruit la possésion et met a jour toutes les informations
	 */
	public void destroyInstantPossession() {
		if(this.state == possession) {
			this.state = fusion;//fusion,
			this.possessEntity = null;
			//changement de l'automate du joueur
			this.getAutomata().getController().setAutomata(this);
		}
	}
	
	/* GETTER ET SETTER des état du joueur */
	
	public int getState() {
		return this.state;
	}
	
	public Shadow getShadow() {
		return this.shadow;
	}
	
	public Entity getPossessEntity() {
		return possessEntity;
	}
	
	public AvatarPlayer getAvatar() {
		return this.avatar;
	}

    public int getMaxArmor() {
    	return this.maxArmor;
    }
	
	public int getCurrentArmor() {
		return this.currentArmor;
	}
	
	public void setCurrentArmor(int newArmor) {
		this.currentArmor = newArmor;
	}

	@Override
	public boolean explode() {
		return false;
	}

	
	public int getCurrentMentalHealth() {
		return currentMentalHealth;
	}
	
	public int getMaxMentalHealth() {
		return maxMentalHealth;
	}


}
