package model.entity;

import info3.game.Game;
import model.Map;
import vue.AvatarZombie;

/**
 * Player : Représentation de l'entité Zombie
 */
public class Zombie extends Enemy {
	/**
	 * Création d'un Zombie
	 * 
	 * @param l Ligne du Zombie
	 * @param c Colonne du Zombie
	 * @param m Map du Zombie
	 */
	public Zombie(int l, int c, Map m) {
		super(l, c, m);
		this.maxHealth = 75;
		this.currentHealth = maxHealth;
		this.avatar = new AvatarZombie(this);

		switch (Game.difficulty) {
		case Game.EASY:
			this.damages = 35;
			break;
		case Game.HARD:
			this.damages = 50;
			break;
		case Game.NUKE:
			this.damages = 150;
			break;
		}
	}

	/**
	 * Déplace le Zombie aux coordonnées indiqués
	 * 
	 * @param l Ligne destination du Zombie
	 * @param c Colonne destination du Zombie
	 */
	@Override
	public void move(int l, int c) {
		this.line = l;
		this.column = c;
	}

	@Override
	public void pop(String str) {
		switch(str) {
		case "N":
		case "F":
			this.moveUp();
			return;
		case "E":
		case "R":
			this.moveRight();
			return;
		case "W":
		case "L":
			this.moveLeft();
			return;
		case "S":
		case "B":
			this.moveDown();
			return;
		default:
			return;
		}
	}

	@Override
	public void wizz(String str) {
		switch(str) {
			case "N":
			case "F":
				super.hitUp();
				return;
			
			case "E":
			case "R":
				super.hitRight();
				return;
				
			case "W":
			case "L":
				super.hitLeft();
				return;
				
			case "S":
			case "B":
				super.hitDown();
				return;
		}
		
	}

	@Override
	public int getTypeEntity() {
		return TypeEntity.ZOMBIE;
	}

	@Override
	public int getRange() {
		return 15;
	}
	
	public boolean isHit() {
		this.currentHealth = 0;
		this.isDead();
		return true;
	}
	
	@Override
	public int getMaxHeath() {
		return this.maxHealth;
	}

	@Override
	public int getCurrentHealth() {
		return this.currentHealth;
	}

	@Override
	public boolean isDead() {
		if(this.currentHealth <= 0) {
			this.deadEntity();
			return true;
		}
		return false;
	}
	
	@Override
	public void setCurrentHealth(int newHealth) {
		this.currentHealth = newHealth;
	}

	@Override
	public void deadEntity() {
		this.getAutomata().getController().getGame().addRemoveEntities(this);
	}

	@Override
	public int hit() {
		return this.damages;
	}

	@Override
	public int isHit(Entity e) {
		return this.currentHealth -= e.damages;
	}

	@Override
	public boolean explode() {
		return false;
	}
}