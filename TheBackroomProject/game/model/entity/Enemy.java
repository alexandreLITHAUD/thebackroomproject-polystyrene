package model.entity;

import automataEngine.AutomatonExe;
import automataEngine.StateExe;
import model.Map;
import model.TypeCase;

/**
 * Enemy : Représentation abstraite d'un ennemi. Un ennemi est un Entity,
 * elle sera sur une case du jeu. Une entité est capable de se déplacer.
 */
public abstract class Enemy extends Entity {
	/**
	 * Création d'une entité
	 * 
	 * @param l Ligne de la case
	 * @param c Colonne de la case
	 * @param m Map sur laquel va évoluer l'entité
	 */
	public Enemy(int l, int c, Map m) {
		super(l, c, m);
	}

	/**
	 * Donne l'automate de l'entité
	 * 
	 * @return l'automate qui controle l'ennemi
	 */
	public AutomatonExe getAutomata() {
		return automata;
	}

	/**
	 * set l'automate de l'entité
	 * 
	 * @param l'automate qui controlera l'ennemi
	 */
	public void setAutomata(AutomatonExe automata) {
		this.automata = automata;
	}

	/**
	 * Donne l'etat courant de l'automate de l'ennemi
	 * 
	 * @return l'etat de l'automate qui controle l'entité
	 */
	public StateExe getCurrentState() {
		return currentState;
	}

	/**
	 * set l'etat courant de l'automate de l'ennemi
	 * 
	 * @param l'etat de l'automate qui controle l'ennemi
	 */
	public void setCurrentState(StateExe currentState) {
		this.currentState = currentState;
	}
	
	public abstract void pop(String str);
	public abstract void wizz(String str);

	/**
	 * Deplace l'ennemi. Le déplacement peut varié en fct de l'ennemi
	 * 
	 * @param l ligne destination du déplacement
	 * @param c colonne destination du déplacement
	 */
	public abstract void move(int l, int c);
	
	/**
	 * Donne le type d'un ennemi
	 * 
	 * @return entier décrivant le type d'ennemi (liste des types dans TypeEntity)
	 * @see TypeCase
	 */
	public abstract int getTypeEntity();
}