package model.entity;

/**
 * TypeEntity : Description des différentes entités du jeu
 */
public class TypeEntity {
	// Enumeration des Type d'Entité
	public static final int JFUSION = 0;
	public static final int PLAYER1 = 1;
	public static final int OMBRE = 2;
	public static final int ZOMBIE = 3; 
	public static final int HOUND = 4;
	public static final int SLIME = 5;
	public static final int MISSILE = 6;
}
