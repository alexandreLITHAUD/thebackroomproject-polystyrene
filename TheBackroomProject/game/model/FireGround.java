package model;

public class FireGround extends Ground {
	/**
	 * Création d'une case de feu
	 * 
	 * @param l Ligne de la case
	 * @param c Colonne de la case
	 */
	public FireGround(int l, int c) {
		super(l, c);
		//isCrossable = true;
	}

	/**
	 * Donne le type d'une case de feu
	 * 
	 * @return entier décrivant le type d'eau (liste des types dans TypeCase)
	 * @see TypeCase
	 */
	@Override
	public int getTypeCase() {
		return TypeCase.FIREGROUND;
	}
}
