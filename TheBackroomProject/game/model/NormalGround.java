package model;

public class NormalGround extends Ground {
	private int mark;
	public static final int noMark = 0;
	public static final int markCross = 1;
	public static final int markRound = 2;
	
	/**
	 * Création d'une case de sol
	 * 
	 * @param l Ligne de la case
	 * @param c Colonne de la case
	 */
	public NormalGround(int l, int c) {
		super(l, c);
		mark = noMark;
		// isCrossable = true;
	}

	/**
	 * Donne le type d'une case de sol
	 * 
	 * @return entier décrivant le type d'eau (liste des types dans TypeCase)
	 * @see TypeCase
	 */
	@Override
	public int getTypeCase() {
		return TypeCase.NORMALGROUND;
	}
	
	/**
	 * Dit si la case est marqué et renvoie le type de mark
	 * 
	 * @return int
	 */
	
	public int getMark() {
		return this.mark;
	}
	
	/**
	 * Set la variable global mark
	 * 
	 * @param m
	 */
	public void setMark(int m) {
		this.mark = m;
	}
	
}