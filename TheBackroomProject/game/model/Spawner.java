package model;

import java.util.Random;

import info3.game.Game;

import model.entity.Entity;
import model.entity.Hound;
import model.entity.Slime;
import model.entity.Zombie;
import model.loot.Armor;
import model.loot.Ammo;
import model.loot.Food;
import model.loot.Heart;
import model.loot.Loot;

/**
 * Ground : Représentation du spawner de mob de la map
 */
public class Spawner {
	/**
	 * Création d'une case spawner
	 * 
	 * @param l Ligne de la case
	 * @param c Colonne de la case
	 */

	private int nbEnemy;

	private int nbLoot = 20;
	
	private Map map;
	private Game g;
	
	
	private int probaSlime = 25;
	private int probaHound = 50;
	
	private int probaZombie = 85;

	private int probaFood = 25;
	private int probaHeart = 25 + probaFood;
	private int probaArmor = 25 + probaHeart;
	
	public Spawner(Game g, Map m) {
		switch (Game.difficulty) {
		case Game.EASY:
			this.nbEnemy = 5;
			break;
		case Game.HARD:
			this.nbEnemy = 10;
			break;
		case Game.NUKE:
			this.nbEnemy = 40;
			break;
		}
		this.map = m;
		this.g = g;
		updateSpawn();
	}
	
	public void updateSpawn() {
		
		Random rand = new Random();
		
		int entityNumber = g.getEntities().size();
		
		for(int i = entityNumber;i < this.nbEnemy;i++) {
			
			int l = 0;
			int c = 0;
				
			do {
				c = rand.nextInt(map.getNbCol());
				l = rand.nextInt(map.getNbLine());
			}while(!this.isCaseLegal(l, c));

			
			int randomValue = rand.nextInt(100)+1;
			Entity e;
			
			if(randomValue < probaSlime) {
				e = new Slime(l,c,map);		
			}else if(randomValue < probaHound) {
				e = new Hound(l,c,map);	
			}else {
				e = new Zombie(l,c,map);	
			}
			
			this.map.getCaseAt(l, c).setGameItem(e);
			this.g.addNewEntities(e);	
		}
		
		// gestion du spawn des loots
		int LootNumber = g.getLoot().size();
		
		for(int i = LootNumber;i < this.nbLoot;i++) {
			
			int l = 0;
			int c = 0;
				
			do {
				c = rand.nextInt(map.getNbCol());
				l = rand.nextInt(map.getNbLine());
			}while(!this.isCaseLegal(l, c));

			
			int randomValue = rand.nextInt(100)+1;
			Loot loot;
			
			if(randomValue < this.probaFood ) {
				loot = new Food(l,c,map);		
			}else if(randomValue < this.probaHeart){
				loot = new Heart(l,c,map);	
			}else if(randomValue < this.probaArmor) {
				loot = new Armor(l,c,map);
			}else {
				loot = new Ammo(l,c,map);
			}

			this.map.getCaseAt(l, c).setGameItem(loot);
			this.g.getLoot().add(loot);		
	
		}
		
		
	}
	
	/**
	 * Retourne le nombre d'entité
	 * 
	 * @return int
	 */
	public int getNbEnemy() {
		return this.nbEnemy;
	}
	
	/**
	 * 
	 * @param l
	 * @param c
	 * @return boolean
	 * 
	 * vérifie si la case n'est pas un mur et qu'il n'y a pas une entité dessus
	 */
	private boolean isCaseLegal(int l, int c) {
		
		return (this.map.getCaseAt(l, c).getTypeCase() == TypeCase.NORMALGROUND || this.map.getCaseAt(l, c).getTypeCase() == TypeCase.FIREGROUND || this.map.getCaseAt(l, c).getTypeCase() == TypeCase.WATERGROUND) && !this.map.getCaseAt(l, c).hasGameItem();
		// TODO AJOUTER POUR NE PAS SPWAN A UNE DISTANCE MIN
		
	}
	
	
	
}