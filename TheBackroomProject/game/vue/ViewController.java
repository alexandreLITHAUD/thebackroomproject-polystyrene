package vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;

import controller.MainController;

import info3.game.CanvasListener;
import info3.game.Game;
import info3.game.Sound;
import info3.game.graphics.GameCanvas;
import info3.game.sound.RandomFileInputStream;

import model.Map;
import model.entity.Entity;
import vue.menu.IFenetre;
import vue.menu.MenuGame;
import vue.menu.NextLevel;
import vue.menu.TitleScreen;
import vue.menu.GameOver;

/**
 * viewController : Controlleur de l'affichage pour les différentes vues du jeu
 */
public class ViewController {
	/* Global */
	private Game game;
	private MainController control;
	/* vue ingame du jeu */
	private ViewPortController vp;
	/* fenetre du jeu */
	private IFenetre fenetre = null;
	
	/* taille actuelle de la fenetre du jeu */
	private int width;
	private int height;

	/* Fenetre */
	JFrame frame;
	JLabel text;
	GameCanvas canvas;
	CanvasListener listener;
	private long textElapsed;

	// Les musiques
	static final int MUSIQUE_DEFAUT = 0;
	static final int MUSIQUE_TITLESCREEN = 1;
	static final int MUSIQUE_DE_FEU = 2;
	static final int MUSIQUES = 3;

	private float volume_music = 0.9F;
	Sound music;
	private int musicIndex;
	private String[] musicNames = new String[] { 
			"default",
			"Backrooms",
			"De_feu" };

	Sound sound;

	// Dimensions de la fenêtre et ses autres propriétés
	public static final int TEXTURE_SIZE = 64; //taille de resize des images
	public static final int CASE_WIDTH = 17;
	public static final int CASE_HEIGHT= 11;
	
	public static final int WINDOW_WIDTH = CASE_WIDTH * TEXTURE_SIZE;
	public static final int WINDOW_HEIGHT= CASE_HEIGHT* TEXTURE_SIZE;

	/**
	 * Création de la vue
	 * 
	 * @param g le game
	 * @param c controller
	 * @param m map (elle est aussi dans game)
	 */
	public ViewController(Game g, MainController c, Map m) {
		this.vp = new ViewPortController(g, m);
		this.game = g;
		this.control = c;
		/* on part sur l'ecran d'acceuil */
		this.titleScreen();
		this.musicIndex = MUSIQUE_TITLESCREEN; // Musique de main title
	}
	
	/**
	 * donne le jeu
	 * 
	 * @return le jeu courant
	 */
	public Game getGame() {
		return this.game;
	}

	/**
	 * gere automates player
	 */
	public int getPlayer1Automate() {
		return this.game.getIDPlayer1Automate();
	}
	
	/**
	 * gere automates projectiles
	 */
	public String[] getAutomatonNames() {
		return this.game.getAutomatonNames();
	}
	
	/**
	 * gere automates projectiles
	 */
	public int getProjAutomateIDof(int e) {
		return this.game.getProjAutomateIDof(e);
	}
	
	/**
	 * gere automates projectiles
	 */
	public int getAutomateIDof(int e) {
		return this.game.getAutomateIDof(e);
	}
	
	/**
	 * gere automates projectiles
	 */
	public String[] getEntityNames() {
		return this.game.getEntityNames();
	}

	/**
	 * gere automates projectiles
	 */
	public void setProjAutomaton(int action) {
		String[] temp = this.game.getAutomatonNames();
		this.control.setProjAutomaton(temp[action]);
	}
	
	/**
	 * gere automates projectiles
	 */
	public void setAutomaton(int e, int a) {
		this.game.setAutomaton(e, a);
	}
	
	/**
	 * gere automates projectiles
	 */
	public void setAutomatePlayer1(int index) {
		this.game.setPlayer1Automaton(index);
	}

	
	/**
	 * demande a swing de lancer le graphisme
	 */
	public void startWindow() {
		// creating the game canvas to render the game,
		// that would be a part of the view in the MVC pattern
		canvas = new GameCanvas(this.control.getListener());

		System.out.println("  - creating frame...");
		Dimension d = new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT);
		frame = canvas.createFrame(d);

		System.out.println("  - setting up the frame...");
		setupFrame();
	}

	/*
	 * Then it lays out the frame, with a border layout, adding a label to the north
	 * and the game canvas to the center.
	 */
	private void setupFrame() {
		frame.setTitle("The Backroom Project");
		frame.setLayout(new BorderLayout());

		frame.add(canvas, BorderLayout.CENTER);

		text = new JLabel();
		text.setText("Tick: 0ms FPS=0");
		frame.add(text, BorderLayout.NORTH);

		// center the window on the screen
		frame.setLocationRelativeTo(null);

		// make the vindow visible
		frame.setVisible(true);
	}

	/**
	 * actualise le jeu
	 * 
	 * @param elapsed temps reel entre 2 ticks
	 */
	public void tick(long elapsed) {
		// Update every second
		// the text on top of the frame: tick and fps
		textElapsed += elapsed;
		if (textElapsed > 1000) {
			textElapsed = 0;
			float period = canvas.getTickPeriod();
			int fps = canvas.getFPS();

			String txt = "Tick=" + period + "ms";
			while (txt.length() < 15)
				txt += " ";
			txt = txt + fps + " fps   ";
			
			if(fenetre == null) {
				txt += "    Etage : "+game.stageNumber+"    Entités : "+game.getEntities().size();
			}
			
			text.setText(txt);
		}
		// Rafraîchit la fenêtre si elle existe, sinon rafraîchit la vue en jeu
		if (this.fenetre != null) {
			this.fenetre.tick(elapsed);
		}
		else {
			for(Entity e:game.getEntities()) {
				e.tick(elapsed);
			}
			this.vp.tick(elapsed);
		}
	}

	/**
	 * paint
	 * 
	 * @param g la toile ou peindre
	 */
	public void paint(Graphics g) {
		// get the size of the canvas
		this.width = canvas.getWidth();
		this.height = canvas.getHeight();

		// erase background
		g.setColor(Color.gray);
		g.fillRect(0, 0, width, height);

		//affiche la bonne version de l'affichage 
		if (fenetre == null) {
			vp.paint(g, width, height);
		} else {
			this.fenetre.paint(g, width, height);
		}
	}
	
	/**
	 * set la map, permet de ne pas recreer toute la vue au changemebt de map
	 * 
	 * @param m la map cpourante
	 */
	public void setMap(Map m) {
		vp.setMap(m);
	}

	/**
	 * set les inputs
	 * 
	 * @param inputs touches actives actuelles
	 */
	public void setInputs(ArrayList<Integer> inputs) {
//		if (inputs.contains((int) 'M')) {
//			vp.switchMode();
//		}
		if (inputs.contains(27)) {// Code ascii de escape
			this.fenetre = new MenuGame(this, this.vp);
		}
	}

	/**
	 * dit si on est en menu
	 * 
	 * @return vrai si on est en menu
	 */
	public boolean inMenu() {
		return fenetre != null;
	}

	/**
	 * Quitte le menu
	 */
	public void leaveMenu() {
		this.fenetre = null;
	}

	public void clickAs(int x, int y) {
		if (this.fenetre != null) {
			this.fenetre.clickAs(x, y);
		}else {
			this.vp.clickAs(x,y);
		}
	}

	/**
	 * distribue les infos souries
	 * 
	 * @param x position en x
	 * @param y position en y
	 * @param button bouton appuyé
	 */
	public void clickReleaseAs(int x, int y, int button) {
		if (fenetre != null) {
			this.fenetre.clickReleaseAs(x, y);
		}else {
			this.vp.clickReleaseAs(x,y,button);
		}
	}
	
	/**
	 * distribue les infos souries
	 * 
	 * @param x position en x
	 * @param y position en y
	 */
	public void mouseMove(int x, int y) {
		if(this.fenetre == null)
			this.vp.mouseMove(x, y);
	}
	
	/*
	 * ================================================================ All the
	 * methods below are invoked from the GameCanvas listener, once the window is
	 * visible on the screen.
	 * ==============================================================
	 */
	public void loadMusic() {
		String filename = "resources/musiques/" + this.musicNames[this.musicIndex] + ".ogg";
		try {
			RandomAccessFile file = new RandomAccessFile(filename, "r");
			RandomFileInputStream fis = new RandomFileInputStream(file);
			canvas.playMusic(fis, 0, this.volume_music);
		} catch (Throwable th) {
			th.printStackTrace(System.err);
			System.exit(-1);
		}
	}
	
	/**
	 * donne le volume
	 * 
	 * @return le volume actuel
	 */
	public float getVolume() {
		return this.volume_music;
	}

	/**
	 * gère le volume
	 */
	public void sOnOffMusic() {
		this.volume_music = (this.volume_music <= 0.0F) ? 1.0F : 0.0F;
		this.loadMusic();
	}

	/**
	 * change le volume
	 * 
	 * @param volume volume souhaité
	 */
	public void changeVolume(float volume) {
		this.volume_music = Math.min(Math.max(volume, 0.0f), 100.0f);
		this.canvas.changeVolume(volume);
	}

	/**
	 * change la musique
	 * 
	 * @param musicIndex index de la musique
	 */
	public void changeMusic(int musicIndex) {
		this.musicIndex = (musicIndex > MUSIQUES || musicIndex < 1) ? MUSIQUE_DEFAUT : musicIndex;
		this.loadMusic();
	}
	
	/**
	 * change la musique
	 * 
	 * @param musicIndex nom de la musique
	 */
	public void changeMusic(String filename) {
		filename = "resources/musiques/" + filename;
		try {
			RandomAccessFile file = new RandomAccessFile(filename, "r");
			RandomFileInputStream fis = new RandomFileInputStream(file);
			canvas.playMusic(fis, 0, this.volume_music);
		} catch (Throwable th) {
			th.printStackTrace(System.err);
			System.exit(-1);
		}
	}
	
	/**
	 * largeur
	 * 
	 * @return taille en largeur de l'ecran
	 */
	public int getWidth() {
		return this.width;
	}
	
	/**
	 * hauteur
	 * 
	 * @return taille en hauteur de l'ecran
	 */
	public int getHeight() {
		return this.height;
	}

	/**
	 * change le stage actuel
	 * 
	 * @param map map actuelle
	 */
	public void newStage(Map map) {
		this.setMap(map);
		this.fenetre = new NextLevel(this);
	}
	
	/**
	 * demande au game de lancer un nv stage 
	 */
	public void newStage() {
		this.game.newStage();
	}
	
	/**
	 * affiche le game over
	 */
	public void gameOver() {
		this.fenetre = new GameOver(this);
	}
	
	public void titleScreen() {
		this.fenetre = new TitleScreen(this);
	}

	/**
	 * set la difficultée
	 */
	public void changeGameDifficulty(int difficulty_choice) {
		this.game.changeGameDifficulty(difficulty_choice);
	}
	
	/**
	 * gere automates projectiles
	 */
	public int getProjAutomateID() {
		String[] temp = this.game.getAutomatonNames();
		String object = this.control.getProjAutomaton();
		for (int i = 0 ; i < temp.length ; i++)
			if (object.equalsIgnoreCase(temp[i]))
				return i;
		return -1;
	}
}