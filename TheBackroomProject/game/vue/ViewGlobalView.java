package vue;

import java.awt.Graphics;
import java.util.ArrayList;

import model.Case;
import model.Map;

/**
 * GlobalView : Gere l'affichage en mode global du jeu
 */
public class ViewGlobalView {
	private Map m;
	private ViewPortController view;
	private int line,col;

	/**
	 * Création d'une vue globale pour l'affichage
	 * 
	 * @param ma Map a afficher
	 */
	public ViewGlobalView(ViewPortController view, Map ma) {
		this.m = ma;
		this.view = view;
		this.line = m.getNbLine();
		this.col = m.getNbCol(); 
	}

	/**
	 * Produit l'affichage du jeu
	 * 
	 * @param g Graphics a peindre
	 * @param l largeur de l'espace d'affichage
	 * @param h hauteur de l'espace d'affichage
	 */
	public void paint(Graphics g, int l, int h) {
		Case c;
		int lCase = l / this.col;
		int hCase = h / this.line;
		// On parcours et affiche de la bonne couleur chaque case
		ArrayList<Case> listeCaseEntity=new ArrayList<>();
		for (int i = 0; i < this.line; i++) {
			for (int j = 0; j < this.col; j++) {
				c = m.getCaseAt(i, j);
				if(c.hasGameItem()) {
					listeCaseEntity.add(c);
				}
				view.paintCase(g, c, j* lCase, i*hCase	, lCase, hCase);
			}
		}
		view.paintEntities(g,lCase, hCase,0,0);
		view.paintBullet(g, lCase, hCase,0,0);

	}

}
