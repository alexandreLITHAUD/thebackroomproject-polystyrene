package vue;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import utils.ImgUtils;

public class Button {
	private int posXButton, posYButton, widthButton, heightButton;
    private boolean click;
    private BufferedImage Bclick, Breal;
    private String title;
    private Font titleFont;

    /**
     * Constructeur du bouton sans titre par défaut
     * Est nécessaire pour créer le bouton du TitleScreen.
     * 
     * @param x
     * @param y
     */
    public Button(int x, int y) {
        this.Bclick = ImgUtils.loadImg("resources/button/start.png");
        this.Breal = ImgUtils.loadImg("resources/button/start_r.png");
        this.title = "";
        this.titleFont = null;
        this.init(x, y);
    }
    
    /**
     * Constructeur classique du bouton.
     * L'image est chargée à partir de button.png
     * Le paramètre String text indique le titre du bouton à afficher.
     * 
     * @param x
     * @param y
     * @param text
     */
    public Button(int x, int y, String text) {
        this.Bclick = ImgUtils.loadImg("resources/button_r.png");
        this.Breal = ImgUtils.loadImg("resources/button.png");
        this.title = text;
        this.titleFont = new Font("resources/consola.ttf", Font.PLAIN, 32);
        this.init(x, y);
    }
    
    /**
     * Constructeur du bouton
     * Le paramètre filename permet de charger une autre image que celle du constructeur classique
     * 
     * @param x
     * @param y
     * @param text
     * @param filename
     */
    public Button(int x, int y, String text, String filename) {
        this.Bclick = ImgUtils.loadImg("resources/"+filename+"_r.png");
        this.Breal = ImgUtils.loadImg("resources/"+filename+".png");
        this.title = text;
        this.titleFont = new Font("resources/consola.ttf", Font.PLAIN, 32);
        this.init(x, y);
    }
    
    /**
     * Initialise ou rafraîchit les données géométriques du bouton en fonction des nouvelles
     * coordonées indiquées
     * 
     * @param x
     * @param y
     */
    private void init(int x, int y) {
        this.posXButton = x;
        this.posYButton = y;
        this.widthButton = this.Bclick.getWidth();
        this.posXButton -= this.widthButton / 2;
        this.heightButton= this.Bclick.getHeight();
        this.posYButton -= this.heightButton/ 2;
    }
    
    /**
     * Réagit à un clic
     * Retourne :
     *  + true , si le clic a touché le bouton
     *  + false, si le clic n'a pas touché le bouton
     * 
     * @param x
     * @param y
     * @return
     */
    public boolean checkClickOn(int x, int y) {
    	this.click = x >= this.posXButton &&
    			x <= this.posXButton + this.widthButton &&
    			y >= this.posYButton &&
    			y <= this.posYButton + this.heightButton;
    	return this.click;
    }
    
    /**
     * Réagit au relâchement du clic
     * Retourne :
     *  + true , si les coordonnées collent avec celles du bouton et que le clic a déjà été enfoncé
     *  		 précédemment dessus
     *  + false sinon
     * @param x
     * @param y
     * @return
     */
    public boolean checkClickToRelease(int x, int y) {
		if (this.click && this.checkClickOn(x, y)) {
			this.click = false;
			return true;
		} else return false;
    }
    
    /**
     * Envoie l'ordre de dessiner le bouton sur le graphique g donné en paramètre
     * en fonction des nouvelles coordonnées (s,t)
     * 
     * @param g
     * @param s
     * @param t
     */
    public void paint(Graphics g, int s, int t) {
    	this.init(s, t);
    	g.drawImage((this.click) ? this.Bclick : this.Breal, 
    			this.posXButton, this.posYButton, 
    			this.widthButton, this.heightButton, null);
    	if (this.titleFont != null) {
        	g.setColor(Color.WHITE);
        	g.setFont(this.titleFont);
        	FontMetrics metrics = g.getFontMetrics();
        	int x = this.posXButton + this.widthButton / 2 - (metrics.stringWidth(this.title) / 2);
        	int y = this.posYButton + this.heightButton/ 2 + (metrics.getHeight() / 4);
        	g.drawString(this.title, x, y);
    	}
    }
}
