package vue.page;

import java.awt.Graphics;

import vue.Button;
import vue.Selector;
import vue.ViewController;
import vue.menu.MenuGame;

public class GameMenuAutomataProj implements IPage {
	private Selector selector;
	private Button button;
	private int wScreen, hScreen;
	private ViewController game;
	private MenuGame inGame;

	/**
	 * Constructeur de la page qui modifie l'automate de tous les prochains projectiles envoyés
	 * par le joueur. 
	 * 
	 * @param game
	 * @param menuGame
	 */
	public GameMenuAutomataProj(ViewController game, MenuGame menuGame) {
		this.game = game;
		this.inGame = menuGame;
		this.wScreen = ViewController.WINDOW_WIDTH;
		this.hScreen = ViewController.WINDOW_HEIGHT;
		this.button = new Button(this.wScreen / 2, 120, "Back to menu");
		if (this.game.getProjAutomateID() >= 0)
			this.selector = new Selector(this.wScreen / 2, this.hScreen / 2,
				"Bullets Automaton", this.game.getAutomatonNames(), 
					this.game.getProjAutomateID());
		else
			this.selector = null;
	}

	@Override
	public void buttonClicked(int x, int y) {
		if (this.selector != null)
			this.selector.clickOn(x, y);
		this.button.checkClickOn(x, y);
	}

	@Override
	public void buttonClickReleased(int x, int y) {
		// The double selector choice
		if (this.selector != null) {
			int action = this.selector.checkClickToRelease(x, y);
			this.game.setProjAutomaton(action);
		}
		// The "Back to main menu" button action
		if (this.button.checkClickToRelease(x, y)) {
			this.inGame.changePage("Main");
		}
	}

	@Override
	public void paint(Graphics g, int l, int h) {
		this.wScreen = l;
		this.hScreen = h;
		if (this.selector != null)
			this.selector.paint(g, l/2, h/2);
		this.button.paint(g, l/2, 120);
	}

}
