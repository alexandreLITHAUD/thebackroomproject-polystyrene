package vue.page;

import java.awt.Graphics;

import utils.Miscellaneous;
import vue.Button;
import vue.Selector;
import vue.ViewController;
import vue.menu.MenuGame;

public class GameMenuMusics implements IPage {

	private Selector selector;
	private Button button;
	private int wScreen, hScreen;
	private ViewController game;
	private MenuGame inGame;

	/**
	 * Constructeur de la page des musiques
	 * 
	 * @param game
	 * @param inGame
	 */
	public GameMenuMusics(ViewController game, MenuGame inGame) {
		this.game = game;
		this.inGame = inGame;
		this.wScreen = ViewController.WINDOW_WIDTH;
		this.hScreen = ViewController.WINDOW_HEIGHT;
		this.button = new Button(this.wScreen / 2, 120, "Back to music menu");
		this.selector = new Selector(this.wScreen / 2, this.hScreen / 2, "Select your music !",
				Miscellaneous.parseFolder("resources/musiques/", ".ogg"), 0);
	}

	@Override
	public void buttonClicked(int x, int y) {
		this.selector.clickOn(x, y);
		this.button.checkClickOn(x, y);
	}

	@Override
	public void buttonClickReleased(int x, int y) {
		this.selector.checkClickToRelease(x, y);
		String filename = this.selector.getItem();
		int action = (this.button.checkClickToRelease(x, y)) ? 0 : -1;
		switch (action) {
		case 0:
			game.changeMusic(filename);
			inGame.changePage("Music");
			break;
		default:
			break;
		}
	}

	@Override
	public void paint(Graphics g, int l, int h) {
		this.wScreen = l;
		this.hScreen = h;
		this.selector.paint(g, l/2, h/2);
		this.button.paint(g, l/2, 120);
	}
}
