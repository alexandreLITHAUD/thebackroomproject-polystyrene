package vue.page;

import java.awt.Graphics;

import vue.Button;
import vue.ViewController;
import vue.menu.MenuGame;

public class GameMenu0 implements IPage {
	private final int BOUTONS = 4;
	private Button[] bouton = new Button[BOUTONS];
	private final int LEAVE_MENU = 0;
	private final int MENU_MUSIC = 1;
	private final int MENU_AUTOMATA = 2;
	private final int MENU_AUTOMATA_PROJ = 3;
	private int wScreen, hScreen;
	private ViewController view;
	private MenuGame inGame;
	
	/**
	 * Constructeur de la page principale du menu en jeu
	 * @param view
	 * @param inGame
	 */
	public GameMenu0(ViewController view, MenuGame inGame) {
		this.wScreen = ViewController.WINDOW_WIDTH;
		this.hScreen = ViewController.WINDOW_HEIGHT;
		this.bouton[0] = new Button(this.wScreen / 2, this.hScreen / 2 - 200, "Back to game");
		this.bouton[1] = new Button(this.wScreen / 2, this.hScreen / 2 - 100, "Manage music");
		this.bouton[2] = new Button(this.wScreen / 2, this.hScreen / 2 + 000, "Manage Entities Automata");
		this.bouton[3] = new Button(this.wScreen / 2, this.hScreen / 2 + 100, "Manage Projectiles Automata");
		this.view = view;
		this.inGame = inGame;
	}
	
	public void buttonClicked(int x, int y) {
		for (int i = 0 ; i < BOUTONS ; i++) {
			if (this.bouton[i].checkClickOn(x, y)) {
				break;
			}
		}
	}
	
	public void buttonClickReleased(int x, int y) {
		int action = -1;
		for (int i = 0 ; i < BOUTONS ; i++) {
			if (this.bouton[i].checkClickToRelease(x, y)) {
				action = i;
				break;
			}
		}
		switch (action) {
		case LEAVE_MENU:
			this.view.leaveMenu();
			break;
		case MENU_MUSIC:
			this.inGame.changePage("Music");
			break;
		case MENU_AUTOMATA:
			this.inGame.changePage("Automata");
			break;
		case MENU_AUTOMATA_PROJ:
			this.inGame.changePage("Bullets");
		default:
			break;
		}
	}
	
	public void paint(Graphics g, int l, int h) {
		this.wScreen = l;
		this.hScreen = h;
		this.bouton[0].paint(g, this.wScreen / 2, this.hScreen / 2 - 200);
		this.bouton[1].paint(g, this.wScreen / 2, this.hScreen / 2 - 100);
		this.bouton[2].paint(g, this.wScreen / 2, this.hScreen / 2 + 000);
		this.bouton[3].paint(g, this.wScreen / 2, this.hScreen / 2 + 100);
	}
}
