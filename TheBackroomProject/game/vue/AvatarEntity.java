package vue;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import model.entity.Entity;

public abstract class AvatarEntity {
	
	protected int m_imageIndex = 0;
	protected long m_imageElapsed = 0;
	public boolean moving;
	
	protected final int NB_IMAGES = 4;
	protected int animMoveSpeed;
		
	static final int SOUTH = 0;
	static final int WEST = 4;
	static final int EAST = 8;
	static final int NORTH = 12;

	protected int countFrame;	//Frames passées depuis le lancement de l'animation
	
	protected Entity entity;
	
	protected BufferedImage[] sprites;
	protected BufferedImage[] iddle;

		
	protected int orientation = 0; //default
	
	public AvatarEntity() {
	}
	
	/**
	 * Vérifie que le sprite existe et l'initialise s'il ne l'est pas
	 */
	abstract public void checkSprite();
	
	
	/**
	 * Setter pour l'orientation de l'avatar lorsque l'entité se déplace
	 * @param ori	string représentant la direction du sprite 
	 */
	public void setOrientation(String ori) {
		switch (ori) {
		case "W":
			//this.orientation = 6;
			this.orientation = WEST;
			break;
		case "N":
			//this.orientation = 12;
			this.orientation = NORTH;
			break;
		case "E":
			//this.orientation = 18;
			this.orientation = EAST;
			break;
		default:
			this.orientation = SOUTH;
		}
		m_imageIndex = orientation;

	}
	
	public int getOrientation() {
		return this.orientation;	
	}
	
	/**
	 * Réalisation de l'animation sur la base des ticks propagés à l'entité
	 * Maintiens à jour le m_imageIndex permettant le choix du sprite
	 * @param elapsed
	 */
	public void tick(long elapsed) {
	if (this.sprites == null) {
		return;
	}
	if (moving) {
	    m_imageElapsed += elapsed;
	    if (m_imageElapsed > animMoveSpeed) {
	      m_imageElapsed = 0;
	      m_imageIndex = (m_imageIndex + 1) % sprites.length;
	      countFrame++;
	    }
	    
	    if (countFrame == NB_IMAGES) {
	    	moving = false;
	    	countFrame = 0;
			m_imageIndex = 0;	//set pour iddle
	    }
	  }
	  else {
		  m_imageIndex = orientation;
	  }
	}
	

	public void setMoving(boolean state) {
		this.moving = state;
	}
	
	abstract public void paint(Graphics g, int x, int y, int dx, int dy);
}
