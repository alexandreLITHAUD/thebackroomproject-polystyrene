package vue;

import java.awt.Color;
import java.awt.Graphics;

import model.weapon.Projectile;

public class AvatarBullet extends AvatarEntity{
	private Projectile bullet;
	
	public AvatarBullet(Projectile bulletParam) {
		this.bullet=bulletParam;
	}
	
	/**
	 * inutile car la balle n'a pas de sprite
	 */
	@Override
	public void checkSprite() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Affiche un carré jaune au coordonné case x,y et utilise percent pour savoir le percent en fonction d'un pourcentage dans la case
	 * 
	 * @param g,x,y,dx,dy
	 */
	@Override
	public void paint(Graphics g, int x, int y, int dx, int dy) {
		g.setColor(Color.yellow);
		g.fillRect(x+(int)(dx*(bullet.percentX/100)), y+(int)(dy*(bullet.percentY/100)), 10, 10);
		
	}

}
