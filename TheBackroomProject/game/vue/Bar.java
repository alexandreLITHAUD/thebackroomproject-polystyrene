package vue;

import java.awt.Color;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Bar {
	private int posX, posY, width, height;
	private int cursorX, cursorWidth, cursorHeight;
    private float rate;
    private BufferedImage cursor, back;
    private String title;
    private Font titleFont;

    /**
     * Constructeur de la jauge (Bar).
     * L'image du curseur est générée à partir de bar_cursor.png
     * Celle du fond sur lequel glisse le curseur est générée à partir de bar.png
     * Le principe d'une jauge est de renvoyer un pourcentage choisi par l'utilisateur.
     * Ce pourcentage est graphiquement calculé : position du curseur par rapport au fond.
     * Si le curseur est au début du fond : 0.0%
     * Si le curseur est à la fin du fond : 100%
     * 
     * Les coordonnées données en paramètre du constructeur indiquent celles du centre du bouton.
     * Le flottant donné en paramètre du constructeur est un réel compris entre 0 et 1 qui
     * indique à quelle position situer le curseur lors de l'initialisation du curseur.
     * 
     * Pour changer la valeur renvoyée par la jauge (Bar), cliquer dans le fond et le curseur
     * se déplace à l'endroit où vous avez cliqué.
     * 
     * @param x
     * @param y
     * @param title
     * @param rate
     */
    public Bar(int x, int y, String title, float rate) {
    	// Récupération des sprites depuis la banque de sprites chargés dans la RAM
    	this.cursor = utils.SpriteBank.getSprite("resources/bar_cursor.png");
    	this.back = utils.SpriteBank.getSprite("resources/bar.png");
    	this.titleFont = utils.SpriteBank.getFont(32);
    	// Quelques données géométriques
    	this.cursorWidth = this.cursor.getWidth();
    	this.cursorHeight= this.cursor.getHeight();
    	this.width = this.back.getWidth();
    	this.height= this.back.getHeight();
    	this.posX = x - this.width / 2;
    	this.posY = y - this.height / 2;
    	// Titre qui sera affiché au centre de la jauge
    	this.title = title;
    	// Jauge de départ
    	this.rate = rate;
    	// Initialisation de la position du curseur
		this.cursorX = (int)(this.rate * (float)(this.width - this.cursorWidth) + (float)this.posX);
    }
    
    /**
     * Rafraîchit les données géométriques de la jauge en fonction des nouvelles coordonnées
     * du centre de la jauge recalculées par la page.
     * 
     * @param x
     * @param y
     */
    private void refreshCoords(int x, int y) {
    	this.cursorWidth = this.cursor.getWidth();
    	this.cursorHeight= this.cursor.getHeight();
    	this.width = this.back.getWidth();
    	this.height= this.back.getHeight();
    	this.posX = x - this.width / 2;
    	this.posY = y - this.height / 2;
    }
    
    /**
     * Réagit à un clic
     * Retourne la valeur de la jauge en fonction des nouvelles (ou anciennes) positions du curseur
     * 
     * @param x
     * @param y
     * @return rate
     */
    public float clickOn(int x, int y) {
    	if (x >= this.posX &&
    			x <= this.posX + this.width &&
    			y >= this.posY &&
    			y <= this.posY + this.height) {
    		this.cursorX = x - this.cursorWidth / 2;
    		this.cursorX = Math.max(this.cursorX, this.posX);
    		this.cursorX = Math.min(this.cursorX, this.posX + this.width - this.cursorWidth);
    		this.rate = (float)(this.cursorX - this.posX) / (float)(this.width - (this.cursorWidth / 2));
    	}
    	return this.rate;
    }
    
    /**
     * Envoie l'ordre de peindre la jauge à l'écran sur le graphique g donné en paramètre
     * x et y sont les nouvelles coordonées de la jauge
     * 
     * @param g
     * @param x
     * @param y
     */
    public void paint(Graphics g, int x, int y) {
    	this.refreshCoords(x, y);
    	g.drawImage(this.back, 
    			this.posX, this.posY, 
    			this.width, this.height, null);
    	g.drawImage(this.cursor,
    			this.cursorX, this.posY,
    			this.cursorWidth, this.cursorHeight, null);
    	g.setColor(Color.WHITE);
    	g.setFont(this.titleFont);
    	FontMetrics metrics = g.getFontMetrics();
    	y = this.posY + this.height/ 2 + (metrics.getHeight() / 4);
    	x = 0 ; try {
        	x = this.posX + this.width / 2 - 
        			(metrics.stringWidth(this.title + 
        					Float.toString(this.rate*100).substring(0, 4)+" %") / 2);
        	g.drawString(this.title + 
        			Float.toString(this.rate*100).substring(0, 4)+" %",
        			x, y);
    	} catch (StringIndexOutOfBoundsException e) {
    		x = this.posX + this.width / 2 - 
        			(metrics.stringWidth(this.title + 
        					Float.toString(this.rate*100)+" %") / 2);
        	g.drawString(this.title + 
        			Float.toString(this.rate*100)+" %",
        			x, y);
    	}
    }
}
