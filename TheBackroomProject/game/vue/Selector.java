package vue;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import utils.ImgUtils;

public class Selector {
	private BufferedImage back, titlecase, buttonDown, buttonDownR, buttonUp, buttonUpR;
	private boolean upClicked = false, downClicked = false;
	private String[] items;
	private int choice, first = 0;
	private int posX, posY, width, height, totalHeight;
	private boolean[] selection = new boolean[3];
	private String title;
	private Font titleFont;
	
	/**
	 * Constructeur d'un sélecteur simple.
	 * Un sélécteur simple associe un élément parmi une liste (items) à un paramètre.
	 * Le titre du paramètre est donné par title en argument du constructeur.
	 * Les coordonnées (x,y) indiquent la position du centre du sélecteur simple.
	 * L'entier choice indique quel est l'ID initial de l'élément associé au paramètre
	 * 
	 * @param x
	 * @param y
	 * @param title
	 * @param items
	 * @param choice
	 */
	public Selector(int x, int y, String title, String items[], int choice) {
		this.items = items;
		this.init(x, y, title, choice);
	}

	/**
	 * Constructeur d'un sélecteur simple.
	 * Un sélécteur simple associe un élément parmi une liste (items) à un paramètre.
	 * Le titre du paramètre est donné par title en argument du constructeur.
	 * Les coordonnées (x,y) indiquent la position du centre du sélecteur simple.
	 * L'entier choice indique quel est l'ID initial de l'élément associé au paramètre
	 * 
	 * @param x
	 * @param y
	 * @param title
	 * @param items
	 * @param choice
	 */
	public Selector(int x, int y, String title, ArrayList<String> items, int choice) {
		this.items = new String[items.size()];
		for (int i = 0 ; i < this.items.length ; i++) {
			String[] temp = items.get(i).split("/");
			this.items[i] = temp[temp.length-1];
		}
		this.init(x, y, title, choice);
	}
	
	/**
	 * Initialise les données géométriques et graphiques du sélecteur simple
	 * 
	 * @param x
	 * @param y
	 * @param title
	 * @param choice
	 */
	private void init(int x, int y, String title, int choice) {
		this.title = title;
		this.titleFont  = new Font("resources/consola.ttf", Font.PLAIN, 32);
		this.titlecase  = ImgUtils.loadImg("resources/button.png");
		this.back       = ImgUtils.loadImg("resources/bar.png");
		this.buttonDown = ImgUtils.loadImg("resources/button/down.png");
		this.buttonDownR= ImgUtils.loadImg("resources/button/down_r.png");
		this.buttonUp   = ImgUtils.loadImg("resources/button/up.png");
		this.buttonUpR  = ImgUtils.loadImg("resources/button/up_r.png");
		this.width = this.back.getWidth();
		this.height= this.back.getHeight();
		this.totalHeight = this.height * 4;
		this.posX = x - this.width / 2;
		this.posY = y - this.totalHeight / 2;
		this.selection[0] = this.selection[1] = this.selection[2] = false;
		this.choice = choice;
	}
	
	/**
	 * Rafraîchit les données géométriques du sélecteur simple en fonction des coordonnées (x,y)
	 * de son centre
	 * @param x
	 * @param y
	 */
	private void refreshCoords(int x, int y) {
		this.width = this.back.getWidth();
		this.height= this.back.getHeight();
		this.totalHeight = this.height * 4;
		this.posX = x - this.width / 2;
		this.posY = y - this.totalHeight / 2;
	}
	
	/**
	 * Retourne l'ID du choix
	 * @return
	 */
	public int getChoice() {
		return this.choice;
	}
	
	/**
	 * Retourne le titre du choix
	 * @return
	 */
	public String getItem() {
		return this.items[this.choice];
	}
	
	/**
	 * Retourne le nombre d'éléments
	 * @return
	 */
	public int size() {
		return this.items.length;
	}
	
	/**
	 * Réagit à un clic.
	 * Doit être appelé à chaque événement de clic !
	 * 
	 * Retourne l'ID du choix actuel
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public int clickOn(int x, int y) {
		// Check if clicked on up button
		this.upClicked = (x >= this.posX && x <= this.posX + this.height &&
    			y >= this.posY && y <= this.posY + this.height) ? true : false;
		// Check if clicked on down button
		this.downClicked = (x <= this.posX + this.width && x >= this.posX + this.width - this.height &&
				y >= this.posY && y <= this.posY + this.height) ? true : false;
		// Check if clicked on one choice
		for (int i = 1 ; i < 4 ; i++) {
			if (x >= this.posX && x <= this.posX + this.width &&
					y >= this.posY + i*this.height && y <= this.posY + (i+1)*this.height) {
				this.selection[i-1] = true;
				break;
			}
		}
    	return this.choice;
    }
	
	/**
	 * Réagît à un relâchement du clic.
	 * Doit être appelé à chaque événement d'un relâchement du clic !
	 * 
	 * Retourne l'ID du choix actuel
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public int checkClickToRelease(int x, int y) {
		// Check if clicked on up button
		if (x >= this.posX && x <= this.posX + this.height && this.upClicked &&
    			y >= this.posY && y <= this.posY + this.height) {
			this.first = (this.first > 0) ? this.first-1 : this.first;
		}
		// Check if clicked on down button
		if (x <= this.posX + this.width && x >= this.posX + this.width - this.height &&
				y >= this.posY && y <= this.posY + this.height && this.downClicked) {
			this.first = (this.first < this.items.length - 2) ? this.first+1 : this.first;
		}
		// Check if clicked on one choice
		for (int i = 1 ; i < 4 ; i++) {
			if (x >= this.posX && x <= this.posX + this.width && this.selection[i-1] &&
					y >= this.posY + i*this.height && y <= this.posY + (i+1)*this.height) {
				this.choice = (this.first + i - 1 < this.items.length) ? 
						this.first + i - 1 : this.choice;
				this.selection[i-1] = false;
				break;
			}
		}
		return this.choice;
	}
    
	/**
	 * Envoie l'ordre de dessiner le sélecteur sur le graphique g donné en paramètre en fonction
	 * des coordonées (s,t) de son centre.
	 * @param g
	 * @param s
	 * @param t
	 */
    public void paint(Graphics g, int s, int t) {
    	this.refreshCoords(s, t);
    	// Get window metrics and calculate absolute y of the first text
    	FontMetrics metrics = g.getFontMetrics();
    	int y = this.posY + this.height/ 2 + (metrics.getHeight() / 4);
    	g.setColor(Color.WHITE);
    	g.setFont(this.titleFont);
    	// Draw titlecase
    	g.drawImage(this.titlecase, 
    			this.posX, this.posY, 
    			this.width, this.height, null);
    	// Write the title
    	int x = this.posX + this.width / 2 - 
    			(metrics.stringWidth(this.title));
    	g.drawString(this.title, x, y);
    	// Draw button for up
    	g.drawImage((this.upClicked) ? this.buttonUpR : this.buttonUp, 
    			this.posX, this.posY,
    			this.height, this.height, null);
    	// Draw button for down
    	g.drawImage((this.downClicked) ? this.buttonDownR : this.buttonDown, 
    			this.posX + this.width - this.height, this.posY,
    			this.height, this.height, null);
    	// Draw the three bars
    	for (int i = 1 ; i < 4 ; i++) {
        	g.drawImage(this.back,
        			this.posX, this.posY + i * this.height,
        			this.width, this.height, null);
    	}
    	// Write the three choices beginning at this.first
    	for (int i = this.first ; i < this.first + 3 ; i++) {
			g.setColor((i == this.choice) ? Color.YELLOW : Color.WHITE);
    		if (i < this.items.length) {
    			x = this.posX + this.width / 2 -
    					(metrics.stringWidth(this.items[i]));
    			g.drawString(this.items[i], x, y + (i+1-this.first) * this.height);
    		}
    	}
    }
}
