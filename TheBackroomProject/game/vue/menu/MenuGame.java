package vue.menu;

import java.awt.Graphics;
import java.util.ArrayList;

import vue.ViewController;
import vue.ViewPortController;
import vue.page.GameMenu0;
import vue.page.GameMenuAutomataMain;
import vue.page.GameMenuAutomataProj;
import vue.page.GameMenuMusics;
import vue.page.GameMenuVolume;
import vue.page.IPage;

public class MenuGame implements IFenetre {
	private IPage page;
	private ViewController game;
	private ViewPortController back;

	/**
	 * Constructeur du menu en game
	 * @param game
	 * @param back
	 */
	public MenuGame(ViewController game, ViewPortController back) {
		this.game = game;
		this.back = back;
		this.page = new GameMenu0(this.game, this);
	}

	public void paint(Graphics g, int l, int h) {
		this.back.paint(g, l, h);
		this.page.paint(g, l, h);
	}

	public void changePage(String pageName) {
		switch (pageName) {
		case "Main":
			this.page = new GameMenu0(this.game, this);
			break;
		case "Music":
			this.page = new GameMenuVolume(this.game, this);
			break;
		case "Automata":
			this.page = new GameMenuAutomataMain(this.game, this);
			break;
		case "Musics":
			this.page = new GameMenuMusics(this.game, this);
			break;
		case "Bullets":
			this.page = new GameMenuAutomataProj(this.game, this);
			break;
		default:
			break;
		}
	}

	public void clickAs(int x, int y) {
		page.buttonClicked(x, y);
	}

	public void clickReleaseAs(int x, int y) {
		page.buttonClickReleased(x, y);
	}

	@Override
	public void sendInputs(ArrayList<Character> inputs) {
		// Nothing to do :)
	}

	@Override
	public void tick(long elapsed) {
		// Nothing to animate :)
	}
}
