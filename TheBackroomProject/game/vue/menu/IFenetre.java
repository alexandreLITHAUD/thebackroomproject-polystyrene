package vue.menu;

import java.awt.Graphics;
import java.util.ArrayList;

/**
 * Interface des fenêtres de menu avec lesquelles on interagit à la souris.
 * Chaque fenêtre a ses propres pages en attribut, mais chaque fenêtre réagit de la même
 * façon avec les événements de la souris qui surviennent.
 */

public interface IFenetre {
	/**
	 * Récupère les coordonnées du clic de souris et les envoie aux différents objets :
	 * sélecteurs, boutons et jauges si elles sont présentes.
	 * Doit être appelé à chaque événement de clic !
	 * @params x : coordonnées du clic
	 * @params y : coordonnées du clic
	 */
    public void clickAs(int x, int y);

	/**
	 * Récupère les coordonnées du relâchement de clic et les envoie aux différents objets :
	 * sélecteurs, boutons et jauges si elles sont présentes.
	 * Doit être appelé à chaque événement de relâchement du clic !
	 * @params x : coordonnées du clic
	 * @params y : coordonnées du clic
	 * La fonction doit traîter les retours des objets et agir en fonction de ces retours.
	 * Exemple : dans GameMenuAutomata, le sélecteur envoie 2, ce qui signifie qu'il faut utiliser
	 * le 3ème automate, la classe Fenetre doit donc exécuter this.game.changeAutomate(2);
	 */
    public void clickReleaseAs(int x, int y);

    /**
     * Récupère les informations sur la fenêtre réelle et donne l'ordre de dessiner les objets dessus
     * @param g : les informations de la fenêtre
     * @param l : la largeur de la fenêtre réelle
     * @param h : la hauteur de la fenêtre réelle
     */
    public void paint(Graphics g, int l, int h);
    
    /**
     * Récupère les inputs au clavier et agit si il y a besoin d'agir, 
     * en fonction des touches entrées.
     * 
	 * Doit être appelé à chaque événement du clavier! 
	 * 
     * @param inputs
     */
    public void sendInputs(ArrayList<Character> inputs);
    
    /**
     * Envoie l'ordre d'avancer le menu de "elapsed" ticks.
     * Doit être appelé à chaque évolution du tick du jeu !
     * @param elapsed : nombre de ticks temporels à avancer
     */
    public void tick(long elapsed);
}
