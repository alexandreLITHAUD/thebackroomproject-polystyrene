package vue.menu;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import info3.game.Game;
import vue.Button;
import vue.Selector;
import vue.ViewController;

public class TitleScreen implements IFenetre {
	// Il n'y a pas besoin d'attribut de type IPage puisqu'il n'y a qu'une page
	// On ne déclare donc que le button de démarrage [Start]
	private Button button;
	// et le sélecteur de difficulté
	private Selector selector;
	// L'instance de jeu en cours
    private ViewController view;
    // Largeur et hauteur de la fenêtre réelle
    private int wScreen = ViewController.WINDOW_WIDTH;
    private int hScreen = ViewController.WINDOW_HEIGHT;
    // Valeur de retour du sélecteur
    private BufferedImage background;
    
    /**
     * Constructeur de l'écran de départ
     * @param view
     */
    public TitleScreen(ViewController view) {
        this.button = new Button(ViewController.WINDOW_WIDTH / 2 , ViewController.WINDOW_HEIGHT - 160);
        String[] difficulties = {"Easy", "Hard", "Nuke"};
        this.selector = new Selector(this.wScreen / 2, this.hScreen / 3,
        		"Difficulty",
        		difficulties, 3-Game.difficulty);
        this.view = view;
		this.background = utils.SpriteBank.getSprite("resources/backgroundTitleScreen.png");
    }
    
    public void clickAs(int x, int y) {
        this.button.checkClickOn(x, y);
        this.selector.clickOn(x, y);
    }

    public void clickReleaseAs(int x, int y) {
        if (this.button.checkClickToRelease(x, y)) {
        	this.view.newStage();
        	this.view.leaveMenu();
        }
        // Retourne la valeur de difficulté sélectionnée
        int difficulty_choice = this.selector.checkClickToRelease(x, y);
        // Paramètre la difficulté
        this.view.changeGameDifficulty(difficulty_choice);
    }

    public void paint(Graphics g, int l, int h) {
    	this.wScreen = l;
    	this.hScreen = h;
		g.drawImage(this.background, 0, 0, wScreen, hScreen, null);
        this.button.paint(g, l/2, h/3);
        this.selector.paint(g, l/2, h-160);
    }

	@Override
	public void sendInputs(ArrayList<Character> inputs) {
		// Nothing to react to :)
	}

	@Override
	public void tick(long elapsed) {
		// Nothing to animate :)
	}
}
