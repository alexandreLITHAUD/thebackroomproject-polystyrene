package vue.menu;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import vue.AvatarPlayer;
import vue.ViewController;

public class NextLevel implements IFenetre {

	private static final long LIFETIME = 1000;
	
	private long age = 0;
	private int wScreen, hScreen;
	private ViewController game;
	private BufferedImage background;
	private AvatarPlayer p;
	
	/**
	 * Constructeur de la page de transition entre deux étages
	 * Contrairement aux autres fenêtres, celle-ci n'a aucune intéraction avec les clics du
	 * joueur, et disparaît au bout d'un certain nombre de millisecondes (LIFETIME)
	 * @param g
	 */
	public NextLevel(ViewController g) {
		this.wScreen = ViewController.WINDOW_WIDTH;
		this.hScreen = ViewController.WINDOW_HEIGHT;
		this.game = g;
		this.p = new AvatarPlayer(this.game);
		this.p.setOrientation("E");
		this.p.setMoving(true);
		this.background = utils.SpriteBank.getSprite("resources/backgroundNextLevel.png");
	}
	
	@Override
	public void clickAs(int x, int y) {
		// Nothing to react to :)
	}

	@Override
	public void clickReleaseAs(int x, int y) {
		// Nothing to react to :)
	}

	@Override
	public void paint(Graphics g, int l, int h) {
		// Le fond
		this.wScreen = l;
		this.hScreen = h;
		g.drawImage(this.background, 0, 0, wScreen, hScreen, null);
		// Oui
		this.p.paint(g, l/2, h/2, 64, 64);
	}

	@Override
	public void sendInputs(ArrayList<Character> inputs) {
		// Nothing to react to :)
	}

	@Override
	public void tick(long elapsed) {
		this.p.tick(elapsed);
		this.age += elapsed;
		if (this.age > LIFETIME) {
			this.game.leaveMenu();
		}
	}

}
