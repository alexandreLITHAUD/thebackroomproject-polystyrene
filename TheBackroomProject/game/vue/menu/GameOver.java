package vue.menu;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import vue.Button;
import vue.ViewController;

public class GameOver implements IFenetre {
	private int wScreen, hScreen;
	private Button restart;
	private Button titleScreen;
	private ViewController game;
	private BufferedImage background;
	
	/**
	 * Constructeur de la fenêtre de Game Over
	 * @param g
	 */
	public GameOver(ViewController g) {
		this.wScreen = ViewController.WINDOW_WIDTH;
		this.hScreen = ViewController.WINDOW_HEIGHT;
		this.restart = new Button(this.wScreen / 2, this.hScreen - 200, "Restart");
		this.titleScreen = new Button(this.wScreen / 2, this.hScreen - 100, "Title Screen");
		this.game = g;
		this.background = utils.SpriteBank.getSprite("resources/backgroundGameOver.png");
	}
	
	@Override
	public void clickAs(int x, int y) {
		this.restart.checkClickOn(x, y);
		this.titleScreen.checkClickOn(x, y);
	}

	@Override
	public void clickReleaseAs(int x, int y) {
		if (this.restart.checkClickToRelease(x, y)) {
			this.game.newStage();
		} else if (this.titleScreen.checkClickToRelease(x, y)) {
			this.game.titleScreen();
		}
	}

	@Override
	public void paint(Graphics g, int l, int h) {
		this.wScreen = l;
		this.hScreen = h;
		g.drawImage(this.background, 0, 0, this.wScreen, this.hScreen, null);
		this.restart.paint(g, l/2, h-200);
		this.titleScreen.paint(g,  l/2 , h - 100);
	}

	@Override
	public void sendInputs(ArrayList<Character> inputs) {
		// Nothing to do here :)
	}

	@Override
	public void tick(long elapsed) {
		// Nothing to animate here :)
	}
}
