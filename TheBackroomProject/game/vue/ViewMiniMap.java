package vue;

import java.awt.Color;

import java.awt.Graphics;
import java.util.ArrayList;

import model.Case;
import model.Map;
import model.entity.Player;

/**
 * MiniMap : Gere l'affichage en mode minimap du jeu
 */
public class ViewMiniMap {
	// taille du viewport
	private ArrayList<Case> liste;
	private int viewPortSizeRayLine, viewPortSizeRayCol;
	private Player focusPlayer;
	private Map m;
	private ViewPortController view;
	// private int line,col; (unused)
	
	private Color filter;

	/**
	 * Création d'une minimap pour l'affichage
	 * 
	 * @param ma  Map a afficher
	 * @param jou Joueur sur lequel ce centrer
	 */
	public ViewMiniMap(ViewPortController view, Map ma) {
		this.viewPortSizeRayLine = view.viewPortSizeRayLine;
		this.viewPortSizeRayCol = view.viewPortSizeRayCol;
		this.focusPlayer = view.focusPlayer;
		this.m = ma;
		this.view = view;
		this.filter = new Color(255,236,51,64);
		/* this.line = m.getNbLine();
		this.col = m.getNbCol();  */
	}


	/**
	 * Produit l'affichage du jeu
	 * 
	 * @param g Graphics a peindre
	 * @param l largeur de l'espace d'affichage
	 * @param h hauteur de l'espace d'affichage
	 */
	public void paint(Graphics g, int l, int h) {
		// System.out.println(""+l+" "+h);
		Case c;
		int lCase = l / m.getNbCol();
		int hCase = h / m.getNbLine();
		ArrayList<Case> listeCaseEntity=new ArrayList<>();
		this.liste = this.m.getViewPortCoord(viewPortSizeRayLine, viewPortSizeRayCol, focusPlayer);

		for (int i = 0; i < m.getNbLine(); i++) {
			for (int j = 0; j < m.getNbCol(); j++) {
				c = m.getCaseAt(i, j);
				if(c.hasGameItem()) {
					listeCaseEntity.add(c);
				}
				view.paintCase(g, c, j* lCase, i*hCase	, lCase, hCase);
				
				// si la case est en dehors du viewport on la floute
			}
		}
		for (Case ca: this.liste) {
			view.colorCase(g, this.filter, ca.getColumn() * lCase, ca.getLine() * hCase	, lCase, hCase);
		}		
		//affichage des entités
		view.paintEntities(g, lCase, hCase,0,0);
		view.paintBullet(g, lCase, hCase,0,0);


	}
}

