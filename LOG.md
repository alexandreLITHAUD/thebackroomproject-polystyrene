# Journal de bord - Groupe 4 : Polystyrène

> Aurélie Avanturier, Antoine Bonfils, Samuel Brun, Romain Guyot, Alexandre Lithaud, Lilian Puech, Corentin Thomazo, Julien Ferrari

- [Journal de bord - Groupe 4 : Polystyrène](#journal-de-bord---groupe-4--polystyrène)
  - [Vendredi 24/06/2022](#vendredi-24062022)
  - [Jeudi 23/06/2022](#jeudi-23062022)
  - [Mercredi 22/06/202](#mercredi-2206202)
  - [Mardi 21/06/2022](#mardi-21062022)
  - [Lundi 20/06/2022](#lundi-20062022)
  - [Vendredi 17/06/2022](#vendredi-17062022)
  - [Jeudi 16/06/2022](#jeudi-16062022)
  - [Mercredi 15/06/2022](#mercredi-15062022)
  - [Mardi 14/06/2022](#mardi-14062022)
  - [Lundi 13/06/2022](#lundi-13062022)
  - [Vendredi 10/06/2022](#vendredi-10062022)
  - [Jeudi 09/06/2022](#jeudi-09062022)
  - [Mercredi 08/06/2022](#mercredi-08062022)
  - [Mardi 07/06/2022](#mardi-07062022)

---
# Vendredi 24/06/2022
## Répartition des tâches
### **Tout le monde :** Réalisation + montage de la vidéo démonstration

## Vidéo démonstration
- Ecriture du script de la vidéo + vérification des points importants
- Prise vidéo du jeu
- Montage de la vidéo

---

---
# Jeudi 23/06/2022
## Répartition des tâches
### **Tout le monde :** Finalisation du code et préparation des démos
### **Romain et Aurélie** : Fin des sprites et des animations des ennemis

## Branches de démo
- Creéation et préparation de branches pour la démo de la vidéo de présentation

## Sprites + animations
- Animation du zombie + hound

---
# Mercredi 22/06/202
## Répartition des tâches
### **Romain :** Animation Slimes + Zombies
### **Lilian :** Musique + choix difficultés + menu choix automates projectile
### **Samuel :** Fix bug + possession
### **Alexandre :** Mise a jour de l'equilibrage du jeu + modification de certain automates
### **Antoine :** Ajout des différentes difficultées dans le jeu
### **Corentin :** Fix de bug de projectiles + bullets
### **Aurélie :** Fin des dessins des sprites du hound
### **Julien :** Ajout du hit avec l'entité possédé

## Animation Ennemis
- Zombie animé 
- Slime animé

## Musique + choix difficultés / automates
- Création d'une musique
- Création d'un choix de menu d'automates pour les projectiles
- Création d'un choix de difficultés

## Bug et possession
- Fix bug affichage
- Destruction de la possession

## Equilibrage + automates
- Modification des certains automates pour rendre le jeu plus jouable

## Choix difficultés 
- Réglage et sélection des différentes difficultées du jeu

## Bug projectiles
- COrrection d'un bug lié à l'attribution d'automates aux projectiles

## Sprites
- Fin du dessin des sprites des hounds

## Hit avec la possession
- Implémentation de la possibilité frapper avec l'entité possédé par l'ombre à l'aide d'un nouvel automate

---
# Mardi 21/06/2022
## Répartition des tâches
### **Romain :** Travail sur les animations 
### **Samuel :** Travail santé mentale et autres
### **Alexandre :** Travail sur les biomes
### **Corentin :** Travail sur les balles
### **Antoine :** Travail sur les doublures + munitions
### **Lilian :** Finit le menu + game over + choix difficultées
### **Aurélie :** Travail sur les sprites des animations des ennemis et de l'ombre
### **Julien :** Travail sur l'implémentation d'une armure pour le joueur

## Animations
- Implémentation de l'iddle du player dans chaque direction
- Changement de couleur des doublures
- Correction d'un bug d'animation lorsque l'ombre est dans les murs
- Implémentation des sprites avec la classe abstraite AvatarEntity
- Clignotement quand joueur touché 

## Santé mentale
- Marquage des cases au sol par l'ombre et le joueur
- Amélioration de l'interface pour avoir des meilleurs info sur l'entité survolée
- Ajout de la santé mentale et du système de vision qui diminue avec cette dernière
- Fix d'un bug sur l'ombre au niveau de l'automate et de ces transitions
- Equilibrage de la vitesse de l'ombre

## Biomes
- Implementation des biome via l'ajout d'une doublure du personnage
- Grace a ceci nous somme capable de changer a la vole le sprite ou les parametres du joueur selon le type de Case sur lequel il marche

## Balles
- Conversion des balles en entités pour leur donner un automate

## Doublures + munition
- Changement de couleur du personnage lorsque sur une case d'un certain type
- Creation du compteur de munition restant et de l'item Ammo pour les munitions
- Consommation des munitions en fonction de l'arme utilisée

## Game Over + choix difficultée
- Ecran de game over implémenté
- Menu choix de difficultées implémenté

## Sprites
- Dessin des sprites de mouvements des ennemis 
- Dessin du sprite du joueur et de l'ombre quand tu ils ne bougent pas

## Armure
- Création de la classe Armor qui étend la classe Loot
- Ajout d'un paramètre armure dans la classe Player

---
# Lundi 20/06/2022
## Répartition des tâches
### **Lilian :** Travail sur l'écran de Game Over
### **Corentin :** Implémentation des armes et des collectibles
### **Alexandre :** Modification et amélioration des automates
### **Samuel :** Meilleure gestion de l'ombre
### **Antoine :** Implémentation des biomes et de leur impact sur le joueur.
### **Romain :** Travail sur les animations.
### **Aurélie :** Travail sur les sprites des ennemis et de l'ombre
### **Julien :** Implémentation de la satiété + correction bug hit

## Game Over
- Premiers tests d'implémentation de l'écran game over.
- Travail sur les sprites des menus.

## Armes et collectibles
- Ajout d'une arme
- Modification du viewPortmap
- Ajout d'un paintInterface pour afficher l'arme sélectionner et les jauge de vie 
- Test du jeu pour trouver des bugs.

## Automates
- Changement des automates : Modification des nom et des implémentations des automates. 
- Rajout des fonction POP et WIZZ dans les entité afin d'avoir un fonctionnement modulaire des automates. 
- Modification des conditions et des actions afin de ne pas gérer l'intégralité des automates que l'on pourrai essayer. Cette amélioration nous permet de ne jamais avoir d'exception ou de problème de compilation peux importe si les fonctions utilisé sont gérer par notre implémentation.

## Ombre
- Possession du joueur par l'ombre et les différentes façon de posséder.
- Fix de bug sur les automates.
- Marquage d'une case.

## Biomes
- Travail sur les biomes et leur génération.
- Effets sur le joueur -> la vitesse du joueur est impacté par le type de la case.

## Animations
- Animation du personnage quand il ne bouge pas.
- Création classe abstraite entre Avatar et Entity.

## Sprites
- Dessin des sprites en mouvement des slimes et des hounds.
- Dessin du premier sprite de zombie.

## Satiété
- Implémentation du système de satiété avec le joueur qui devient affamé et finit par perdre de la vie.
- Correction de bug avec le hit.

---
# Vendredi 17/06/2022
## Répartition des tâches
### **Lilian :** Correction d'un bug sur le chargement des automates en jeu.
### **Samuel et Alexandre :** Mise en place de la séparation entre le joueur et son ombre.
### **Antoine et Julien :** Implémentation du hit avec un système de pv et de mort.
### **Romain :** Réaction du sprite en cas de collision et centralisation de la gestion des sprites du joueur dans son avatar.
### **Corentin :** Affichage barre de vie et satiété.
### **Aurélie :** Dessin des sprites des ennemis hounds et zombie.

## Correction bug chargement automates
- Soucis lors du chargement d'automate d'un slime dans une autre entité à cause du egg.

## Création de l'ombre
- Création de la classe d'ombre, ajout des méthodes de déplacement des ombres, ajout de l'automate de l'ombre, ajout du système de séparation joueur/ombre.

## Implémentation du hit 
- Amélioration des automates avec l'action Hit, ajout des caractéristiques et des méthodes abstraites dans la classe entity et des méthodes isDead pour tester la mort d'entités.

## Amélioration du sprite
- Blocage de l'animation si mouvement impossible
- Orientation dynamique du personnage même en cas de collision
- Modification de l'implémentation des animations pour gérer les sprites dans la classe Avatar de Player

## Affichage vie et satiété
- Mise en place d'un affichage de jauge de vie et de satiété player
ajout de Loot nourriture et coeur, gestion de leur affichage
implémentation de la fonction pick pour que le player récupère les loots, le soigne et lui rend de la vie

## Dessin des sprites
- Ajout des sprites pour les animations des ennemis + gestion d'un tableau de sprites.

---
# Jeudi 16/06/2022
## Répartition des tâches
### **Lilian :** Amélioration menu
### **Romain :** Animation
### **Samuel :** Sélection de case
### **Antoine :** Différents grounds
### **Corentin :** Tir libre
### **Alexandre :** Egg
### **Julien :** Caractéristiques
### **Aurélie :** Sprites monstres

## Amélioration menu
- Menu de transfert d'automates entièrement fonctionnel.
- Possibilité de régler la musique avec la barre coulissante.

## Animation
- Ajout de frames d'animations pour rendre le déplacement du personnage plus fluide.

## Sélection de case
- Sélection de case à la souris notamment pour le système de visée.

## Différents grounds
- Ajout de blocs d'eau et de feu sur le sol, qui pourront avoir un effet sur les entités qui passent dessus.

## Tir libre
- Implémentation d'un système de tir libre via sélection de case.

## Egg
- Ajout de l'action Egg dans les automates avec un slime qui se multiplie à l'infini.

## Caractéristiques
- Ajout des attributs des entités avec la classe abstraite Entity : la vie, la stamina, les dégats etc.

## Sprites 
- Sprites du player terminés.
- Sprites des monstres slimes et hounds commencés.

### ***Problèmes rencontrés :*** 
- Problème de gestion des coordonnées via le thor.

### *Planification des tests :*
- Implémentation des actions hits pour tester les dégats et la bonne réaction des entités.

---
# Mercredi 15/06/2022
## Répartition des tâches
### **Tout le monde :** Code review
### **Alexandre, Samuel et Antoine :** CondKey
### **Aurélie :** Sprites
### **Corentin et Romain :** Animations
### **Julien :** Automates joueurs + ennemis
### **Lilian :** Menu
### **Alexandre, Antoine et Samuel :** Spawner et Enemy
### **Tout le monde :** Merge et test 

## Code review 
- Lilian a expliqué le code de son menu, notamment l'interface Fenêtre, ainsi que Bouton, Bar et Selector.
- Samuel a expliqué le refactory du code afin de mieux respecter le Modele Vue Controlleur.

## CondKey
- Implémentation de la condition Key pour les automates afin de contrôler le joueur via un automate.

## Sprites 
- Dessin des nouvelles orientations pour les animations des sprites

## Animation 
- Création des prototypes de fonction pour les animations par Corentin et implémentation de ces prototypes par Romain.
  
## Automates joueurs + ennemis
- Réalisation de nouveaux automates en .gal afin de pouvoir tester le comportement des ennemis ainsi que des joueurs.

## Menu
- Sélection des fichiers automates .gal  et amélioration du menu en cohérence avec le Modèle Vue Contrôleur.

## Spawner et Enemy 
- Concertation pour l'implémentation d'un spawner d'ennemis et attribution d'automates aux entités crées. Contrôle du nombre d'entités crées avec pourcentage de chance de spawn des classes d'ennemis.

## Merge et test
- Fusion du travail de la veille fournit par Antoine, et du travail de refractorying de Samuel. Alexandre a mis en place une branche master_candidate, sur lequel, la fusion précédemment citée, sera mergée.
- A partir d'ici, tout le monde travaille à partir de masterCandidate

---
# Mardi 14/06/2022
## Répartition des tâches
### **Julien :** Amélioration des bots "Patrouilleur" et "Explorateurs" + Test des automates avec la Vue, le AutomataEngine et le Model.
### **Antoine :** Travail sur le spawn des entités.
### **Samuel, Alexandre, Lilian :** Refactory du game + Merge des nouvelles fonctionnalitées du menu.
### **Lilian :** Amélioration du menu avec possibilités de sélectionner des automates.
### **Romain, Corentin et Samuel :** Révision du Model Vue Controlleur et conception des interactions entres les entités et leur avatar. (débat sur graphe d'objet)
### **Aurélie :** Dessin des sprites au format 64x64 cases.
### **Tout le monde :** Précision du contrat.

## Amélioration bots + Test Automates - Modèle - Vue 
- Amélioration des bots avec l'ajout de conditions chaînés dans les automates.
- Tests d'automates à plusieurs états avec conditions chaînés en regardant le  comportement dans l'interface graphique.

## Spawn des entités 
- Apparition aléatoire des ennnemis dans le labyrinthe.
- Apparition seulement sur des cases vides.

## Refactory de game + merge du menu
- Continuation du refactory du game et surtout de la vue qui, avant était gérée dans le game et maintenant géré dans des classes à part pour un meilleur MCV et donc une meilleure visibilitée.

## Amélioration du menu 
- Possibilité de sélectionner des musqiues et de charger différents fichiers .gal pour toujours plus d'automates

## Révision Model Vue Controlleur
- Commence à penser au côté Vue avec les animations des sprites via la classe Avatar.
- A quoi peut accéder la classe Avatar ? A quoi doit accéder la classe Avatar ? Etc...

## Dessin des sprites
- Dessin du personnage principal dans plusieurs positions pour les animations

## Rajout d'éléments manquants dans le contrat
- Rajout du viewport
- Rajout de la possibilité de sélectionner et d'affecter des automates à des entités différentes
  
## ***Plannification des tests*** 
- Test du Modèle avec la Vue et le Controleur **après le refactory** via le contrôle d'un bot avec un automate
- Test de la génération de nouveaux labyrinthes lorsque l'on prend le portail

---
# Lundi 13/06/2022
## Répartition des tâches
### **Alexandre :** Création d'un automate "suiveur" et changement des conditions Cell et Closest des automates.
### **Antoine :** Amélioration pour simplifier le labyrinthe et implémentation d'une classe ennemy.
### **Lilian :** Ajout de la feature menu principal/écran d'accueil + sélection musique.
### **Samuel :** Refactory de la classe game.
### **Corentin :** Implémentation d'un algorithme de plus court chemin pour trauer les entités dans le labyrinthe.
### **Aurélie :** Travail sur le sprite du perso principal + murs.
### **Romain :** Travail sur les automates et le parser.
### **Julien :** Travail sur la création de bots via les automates.
### **Tout le monde :** Rédaction au propre du proposal pour faire le contrat + Review de code.

## Création d'un automate suiveur :
- Création d'un petit automate qui passe dans le parser et qui fait que le J2 suit le J1 tant qu'il est à moins d'un certain nombre de cases.

## Changement Cell et Closest :
- Problème de coordonnées négatives dû au tore du labyrinthe.

## Amélioration du labyrinthe :
- Possibilité de choisir la largeur des couloirs pour créer des labyrinthes moins complexes.

## Implémentation ennemie :
- Travail sur un système de spawn d'entités ennemies au hasard dans le labyrinthe.

## Feature écran d'accueil / menu principal :
- Ajout de boutons de séléction via menu déroulant dans l'écran d'accueil avec réglage du volume sonore.

## Sélection des musiques :
- Possibilité de choisir sa propre musique dans le jeu via l'importation de .ogg dans le menu principal.

## Refactory de game :
- La classe game étant pour l'instant le coeur de notre implémentation, cette dernière risque de croître très rapidement en taille et en complexité. Un refactory de la classe pour la rendre plus modulaire semble donc nécessaire.
   
## Plus court chemin :
- Réalisation d'un algorithme de plus court chemin de n'importe quelle case vers le joueur afin de permettre aux bots de le poursuivre de manière efficace.

## Sprite perso principal + murs :
- Dessins des sprites et de leurs animations pour commencer à implémenter la Vue de notre jeu.

## Automates + Parser :
- Compréhension plus approfondie du parser pour voir les limites des automates et anticiper les problèmes de features liés à ces derniers.

## Création de bots :
- Implémentation en .gal de nouveaux comportements tel que le Patrouilleur, le SuiveurPatrouilleur, l'Explorateur etc.

## Review de code :
- Review de code du labyrinthe "simplifié" pour pouvoir choisir la largeur des couloirs
- Review de code de l'implémentations des automates avec leur génération via l'AST fourni par le Parser.

## Rédaction du contrat : 
- Mise à jour du Proposal.md avec ajout/suppression de certaines features en accord avec toute l'équipe.

---
# Vendredi 10/06/2022
## Répartition des tâches
### **Alexandre, Samuel, Corentin, Romain et Julien :** Implémentation des automates.
### **Antoine :** Simplification du labyrinthe.
### **Aurélie :** Travail sur les sprites.
### **Lilian :** Implémentation du menu principal.

## Implémentation des automates
- Création d'un petit automate qui fait move les joueurs en permanence vers le haut.
- Travail de compréhension sur les automates et implémentation de nouvelles classes afin de créer des automates à partir du parser et de l'AST.

## Simplification du labyrinthe
- Elargissement des chemins du labyrinthe pour rendre la navigation plus facile.

## Travail sur les sprites
- Présentation des sprites par Aurélie au reste de l'équipe.

## Menu Pause
- Ajout d'une musique d'accueil et d'une barre pour régler le volume.
- Ajout d'un menu de pause sur pression d'un bouton

### ***Problèmes rencontrés :***
- Implémentation de key dans les automates.
- Calcul des nouvelles coordonnées pour la création du labyrinthe.
---
# Jeudi 09/06/2022
## Répartition des tâches
### **Antoine, Samuel et Alexandre :** Intégration + merge du labyrinthe généré aléatoirement avec le master.
### **Corentin, Julien, Romain, Lilian :** Travail de compréhension sur les automates.
### **Aurélie :** Travail sur le style de la page d'accueil.
### **Tout le monde :** Réunion avec M. Périn pour présenter le descriptif du jeu et avoir un premier retour sur nos idées.
### **Tout le monde :** Revue de code pour que tout le monde comprenne le fonctionnement du viewport, du déplacement du joueur et de la génération du labyrinthe.

## Intégration de la génération aléatoire du labyrinthe au GIT
- Refactory du code de génération aléatoire de labyrinthe et résolution des conflits du merge.

## Compréhension automates
- Mise en commun de l'implémentation des automates de M. Périn avec le jeu du cowboy de M. Gruber dans un même projet sous Eclipse. On pourra ainsi tester les automates en les attribuants au comboy.

## Style page d'accueil 
- Travail sur le design du logo et de l'écran d'accueil du jeu où l'on poura faire le menu de sélection du jeu.

## Revue de code
- Présentation par Samuel et Antoine de leur code au reste de l'équipe afin que tout le monde comprenne comment marche la gestion du déplacement des deux joueurs au clavier, les différents modes d'affichage du viewport et de la map ainsi que la génération aléatoire du labyrinthe. 

## Réunion avec M. Périn 
- Discussion sur la complexité du labyrinthe -> possibilité de le "simplifier"
- Discussion sur la possibilitée d'avoir plusieurs entités par case -> nouvelle gestion des collisions.

### ***Problèmes rencontrés :***
- Difficultées pour "rentrer" dans le code du parser des automates.
- Difficultées pour savoir par où commencer l'implémentation d'un premier automate.

---
# Mercredi 08/06/2022
## Répartition des tâches
### **Corentin + Lilian :** implémentation du tir libre.
### **Alexandre + Samuel :** implémentation + merge de la gestion de deux personnages au clavier et du viewport.
### **Antoine + Julien + Romain + Corentin + Aurélie :** lecture de la faq et des automates en GAL. 
### **Tout le monde :** Création dépot git + pull sur dépots locaux.
### **Tout le monde :** Définition du coding style.

## Implémentation du tir libre
- Nous avons implémenté une première version de tir libre, c'est à dire que le personnage évolue dans un monde case par case mais il peut tirer dans toutes les directions, à 360°.

## Merge du viewport + deux personnages déplacés au clavier
- Nous avons fait les premiers merges sur la branche master du dépôt distant en utilisant la technique du "safe merge" vue en cours. On commence par cloner la branche des features en train d'être développées. Ensuite, on se place sur cette branche et on tente de merge le master dessus. Si cela réussit, on fait le merge sur le master. Sinon on résout les conflits sur la branche et si la branche est cassée, on revient au développement sur la branche originale.

## Lecture FAQ + Automates GAL
- Nous avons ensuite lu la FAQ et regardé le parser afin de comprendre comment générer un automate à partir d'un AST.

## Création du dépot git distant + pull sur dépots locaux
- Nous avons créé le dépôt git distant sur lequel nous avons configuré notre gitignore. Nous avons ensuite tous cloné le dépôt git distant sur nos machines en local. Nous avons importé le projet sur Eclipse et vérifier que l'arborescence des répertoires git était bien homogène afin d'éviter les conflits.

## Coding style
- Nous avons ensuite discuté du coding style. Nous avons fait une réunion pour décider ensemble la manière de nommer les variables, gérer la visibilité des classes, de leurs attributs et de leurs méthodes. Également la position des accolades et la structure des if, then, else, while, switch, case, break et for. Pour le reste, on laisse la configuration de Eclipse par défaut pour formater le code avec CTRL+SHIFT+F et CTRL+I.

### ***Problèmes rencontrés :*** 
- Gérer la cohérence entre vue et modèle lors de la collision d'une balle avec une entité.
- Faire le refactory du code pour règler les problèmes de merge.

---
# Mardi 07/06/2022
## Répartition des tâches
### **Alexandre :** Création d'un UML pour le diagramme de classes.
### **Samuel :** Gestion de deux entitées players sur le même clavier.
### **Antoine :** Génération aléatoire du labyrinthe.
### **Tout le monde :** Discussion du jeu de base et des features à mettre dans le Proposal.md
### **Tout le monde :** Discussion autour de l'automate du J1 et du J2.

## Gestion de deux entités au clavier : 
- Samuel a réussi à gérer l’utilisation simultanée du clavier par deux joueurs.

## Génération aléatoire du labyrinthe :
- Antoine a réussi à coder un générateur aléatoire de labyrinthe à l'aide d'un algorithme d'exploration qui créé des chemins en cassant des murs et permet de s'assurer que toutes les cases vides sont accessibles par au moins un chemin.

## Création UML :
- Alexandre a dessiné un premier diagramme de classe pour se mettre d'accord sur l'implémentation, faire des refactory du code au plus tôt et voir comment réaliser nos features sur cette base.
- 
## Discussion du jeu de base 
- Nous avons discuté des mécaniques du jeu et nous nous sommes mis d’accord sur une description du jeu rapide ainsi que sur certains aspects techniques.
- Nous avons choisi de partir sur des features “simples” à implémenter afin d’avoir un jeu fonctionnel qui répond au cahier des charges.
Pour ce faire, nous avons trié nos différentes idées par ordre de priorité.
- Nous avons aussi pu définir plus précisément le rôle de chaque joueur selon le mode de jeu ainsi que les différents périphériques impliqués (souris/clavier) pour telles ou telles actions.
- Nous avons ensuite fait une liste des features obligatoires qui nous semble compliquées afin de pouvoir se les répartir et s’entraîner à les coder.

## Automate des joueurs
- Nous avons commencé à discuter de l’automate du joueur et des différentes problématiques de ce dernier.

### ***Problèmes rencontrés :*** 
- Le fait de pouvoir choisir l’orientation du personnage quand il frappe, sans que ce dernier ne se déplace.
- Le concept doit être bien clair pour chaque membre du groupe.