# Groupe 4 Project - TheBackroomProject - Polystyrene

> Aurélie Avanturier, Antoine Bonfils, Samuel Brun, Romain Guyot, Alexandre Lithaud, Lilian Puech, Corentin Thomazo.

---

# Liste des documents importants :

- ## [Description du Jeu](PROPOSAL.md) (PROPOSAL.md)

- ## [Journal de Bord Projet](LOG.md) (LOG.md)

- ## [Style de Codage de Groupe](CODING_STYLE.md) (CODING_STYLE.md)

- ## [Pourcentages de participations de l'équipe](Pourcentage_participation_equipe.md) (Pourcentage_participation_equipe.md)

---
# Vidéo présentation du jeu sur youtube : https://youtu.be/esxnviw0oq8 

